﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ToBoldlyPlay.Search
{
	/// <summary>
	/// Filter an enumeration of values using string keys.
	/// </summary>
	/// <typeparam name="T">Enumeration value type.</typeparam>
	public class StringFilter<T>
	{
		private IDictionary<string,T> dictionary;

		private SortedDictionary<int, string> candidates;

		private string exactMatch;
		private bool hasExactMatch;

		public string ExactMatch
		{
			get
			{
				return exactMatch;
			}
		}

		public string BestMatch
		{
			get
			{
				if ( hasExactMatch )
				{
					return exactMatch;
				}
				else if ( candidates.Count > 0 )
				{
					return candidates[candidates.Keys.Min()];
				}

				return null;
			}
		}

		public bool HasExactMatch
		{
			get
			{
				return hasExactMatch;
			}
		}

		public bool HasBestMatch
		{
			get
			{
				return dictionary.Count > 0;
			}
		}

		public IList<string> Candidates
		{
			get
			{
				var list = new List<string>();

				foreach ( var @string in candidates.Values )
				{
					list.Add( @string );
				}

				return list;
			}
		}

		private void PreProcess ( IEnumerable<T> values, Func<T, string> mapping )
		{
			dictionary = new Dictionary<string, T>( values.Count() );

			foreach ( var value in values )
			{
				dictionary.Add( mapping( value ), value );
			}
		}

		private void PreProcess ( IDictionary<T, string> dictionary )
		{
			this.dictionary = new Dictionary<string, T>();

			foreach ( var pair in dictionary )
			{
				this.dictionary.Add( pair.Value, pair.Key );
			}

		}

		private void PreProcess ( IDictionary<string, T> dictionary )
		{
			this.dictionary = dictionary;
		}

		private void PreProcess ( IEnumerable<T> values, IEnumerable<string> strings )
		{
			dictionary = new Dictionary<string, T>();

			var enumeratorValues = values.GetEnumerator();
			var enumeratorStrings = strings.GetEnumerator();

			while ( enumeratorValues.MoveNext() && enumeratorStrings.MoveNext() )
			{
				dictionary.Add( enumeratorStrings.Current, enumeratorValues.Current );
			}
		}

		private StringFilter ()
		{
			candidates = new SortedDictionary<int, string>();
		}

		public StringFilter ( IEnumerable<T> enumerable ) : this( enumerable, x => x.ToString() ) { }

		public StringFilter ( IDictionary<string, T> dictionary ) : this()
		{
			PreProcess( dictionary );
		}

		public StringFilter ( IDictionary<T, string> dictionary )
		{
			PreProcess( dictionary );
		}

		public StringFilter ( IEnumerable<T> values, IEnumerable<string> strings ) : this()
		{
			PreProcess( values, strings );
		}

		public StringFilter ( IEnumerable<T> values, Func<T, string> mapping ) : this()
		{
			PreProcess( values, mapping );
		}

		public T GetValue ( string @string )
		{
			if ( dictionary.ContainsKey( @string ) )
			{
				return dictionary[@string];
			}

			return default( T );
		}

		public void Parse ( string input )
		{
			candidates.Clear();

			exactMatch = null;

			hasExactMatch = false;

			if ( string.IsNullOrEmpty( input ) )
			{
				var i = 0;

				foreach ( var @string in dictionary.Keys )
				{
					candidates.Add( i++, @string );
				}

				return;
			}

			var input_lower = input.ToLower();

			foreach ( var @string in dictionary.Keys )
			{
				var string_lower = @string.ToLower();

				int n = Math.Min( input.LevenshteinDistance( @string ), input.LevenshteinDistance( @string ) );

				if ( n < 0 )
				{
					n += Math.Abs( @string.Length - input.Length );

					while ( candidates.ContainsKey( n ) )
					{
						n += Math.Abs( @string.Length - input.Length ) + 1;
					}

					candidates.Add( n, @string );
				}
			}
		}

		public void Clear ()
		{
			Parse( "" );
		}
	}
}
