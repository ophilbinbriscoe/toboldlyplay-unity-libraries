﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ToBoldlyPlay
{
    /// <summary>
    /// A GameObject pooling solution.
    /// </summary>
    public class Pool : MonoBehaviour, IGet<GameObject>
    {
        public enum ExpansionPolicy
        {
            NONE,       // Best performance, no headroom. Should only be used if / when a hard limit is established.
            INCREMENT,  // Guaranteed to only use as much memory as necessary, but will be slow if expanding often.
            DOUBLE      // Grows rapidly, can quickly become far larger than necessary.
        }

        [Tooltip( "The GameObject to be instantiated." )]
        public GameObject prefab;

        [Tooltip( "How many instances will be created at start-up." )]
        [SerializeField] private int initialSize;

        [Tooltip( "How to handle requests when no more instances remain." )]
        [SerializeField] private ExpansionPolicy policy = ExpansionPolicy.DOUBLE;

#if UNITY_EDITOR
        [Header( "Organization" )]

        [Tooltip( "Whether or not instances should be nested under the Pool's transform in the scene." )]
        [SerializeField] private bool nested = true;

        [Tooltip( "Whether or not instances should be hidden in the Scene View.")]
        [SerializeField] private bool hidden = true;
#endif

        // How many instances exist.
        [SerializeField][HideInInspector] private int size;

        // Array of instances.
        [SerializeField][HideInInspector] private GameObject[] instances;

        // The index of the next available instance in the instances array
        private int head;

        void Awake ()
        {
            size = initialSize;

            // Create array
            instances = new GameObject[size];

            // Fill array
            for ( int i = 0; i < size; i++ )
            {
                instances[head++] = Instantiate();
            }
        }

        bool IsEmpty ()
        {
            return head == 0;
        }

        GameObject Instantiate ()
        {
            // Instantiate the prefab
            var instance = Instantiate( prefab );

            instance.SetActive( false );

#if UNITY_EDITOR
            // Nest the new instance under the Pool's transform, if requested
            if ( nested )
            {
                instance.transform.parent = transform;
            }

            // Hide the new instance in the Scene View, if requested
            if ( hidden )
            {
                instance.hideFlags = HideFlags.HideInHierarchy;
            }
#endif

            return instance;
        }

        GameObject Show ( GameObject resource )
        {
            resource.hideFlags = HideFlags.None;

            return resource;
        }

        public GameObject Get ()
        {
            // if there are free instances
            if ( head > 0 )
            {
                // return the top one
                return Show( instances[--head] );
            }

            switch ( policy )
            {
            case ExpansionPolicy.NONE:

                // Notify the developer that the limit was reached
                throw new InvalidOperationException( "Pool is empty and cannot expand. Specify a higher initial allocation." );

            case ExpansionPolicy.INCREMENT:

                // Allocate a spot for a new resource
                instances = new GameObject[size++];
                break;

            case ExpansionPolicy.DOUBLE:

                // Allocate spots for twice as many instances
                instances = new GameObject[size *= 2];
                break;

            default:
                break;
            }

            // Return the new resource
            return Instantiate( prefab );
        }

        public void Release ( GameObject resource )
        {
            // Deactivate the resource
            resource.SetActive( false );

            // If there is a spot for the resource
            if ( head < size )
            {

#if UNITY_EDITOR
                if ( hidden )
                {
                    resource.hideFlags = HideFlags.HideInHierarchy;
                }
#endif

                // Add it to the pool
                instances[head++] = resource;
            }
            else
            {
                // Destroy the resource
                Destroy( resource );
            }
        }
    }
}
