﻿using System;
using System.Collections;
using UnityEngine;

namespace ToBoldlyPlay
{
    public interface ICoroutine : IRun, IHalt, IReset, IDoneEvent { }

	public class UCoroutine : ICoroutine
	{
		public event Action<IDoneEvent> OnDone;

		private class Dispatcher : MonoBehaviour
		{
			void Update ()
			{
				DispatchAll( DispatchPolicy.UPDATE );
			}

			void LateUpdate ()
			{
				DispatchAll( DispatchPolicy.LATE_UPDATE );
			}

			void FixedUpdate ()
			{
				DispatchAll( DispatchPolicy.FIXED_UPDATE );
			}
		}

		private static Dispatcher dispatcher;

		private static UCoroutine slowUpdater;

		private static IEnumerable SlowUpdateCoroutine ()
		{
			while ( true )
			{
				DispatchAll( DispatchPolicy.SLOW_UPDATE );
				yield return null;
			}
		}

		private static IEnumerable AutoCoroutine ( Action action )
		{
			while ( true )
			{
				action();
				yield return null;
			}
		}

		private static IEnumerable AutoCoroutine ( Action action, int n )
		{
			for ( int i = 0; i < n; i++ )
			{
				action();
				yield return null;
			}

			yield break;
		}

		[RuntimeInitializeOnLoadMethod]
		private static void Initialize ()
		{
			dispatcher = new GameObject( "CoroutineDispatcher" ).AddComponent<Dispatcher>();
			dispatcher.gameObject.hideFlags = HideFlags.HideAndDontSave;

			slowUpdater = Run( SlowUpdateCoroutine );
		}

		protected IEnumerator enumerator;

		protected readonly Func<IEnumerable> coroutine;
		
		private DispatchPolicy policy = DispatchPolicy.UPDATE;
		
		/// <summary>
		/// This coroutine's dispatch policy.
		/// </summary>
		public DispatchPolicy Policy { get { return policy; } }

		private bool hasStarted;
		private bool isRunning;
		private bool isDone;

		/// <summary>
		/// True, if this coroutine has been started.
		/// </summary>
		public bool HasStarted
		{
			get
			{
				return hasStarted;
			}
		}

		/// <summary>
		/// True, if this coroutine is not suspended.
		/// </summary>
		public bool IsRunning
		{
			get
			{
				return isRunning;
			}
		}

		/// <summary>
		/// True, if this coroutine has exhausted its enumerator.
		/// </summary>
		public bool IsDone
		{
			get
			{
				return isDone;
			}
		}

		private static event Func<bool> Update;
		private static event Func<bool> LateUpdate;
		private static event Func<bool> FixedUpdate;
		private static event Func<bool> SlowUpdate;

		private static void DispatchAll( DispatchPolicy policy )
		{
			switch ( policy )
			{
			case DispatchPolicy.UPDATE:
				if ( Update != null ) { Update();}
				break;

			case DispatchPolicy.LATE_UPDATE:
				if ( LateUpdate != null ) { LateUpdate(); }
				break;

			case DispatchPolicy.FIXED_UPDATE:
				if ( FixedUpdate != null ) { FixedUpdate(); }
				break;

			case DispatchPolicy.SLOW_UPDATE:
				if ( SlowUpdate != null ) { SlowUpdate(); }
				break;
			}
		}

		/// <summary>
		/// An enumeration of dispatch policies supported by this implementation.
		/// </summary>
		public enum DispatchPolicy
		{
			UPDATE,	
			LATE_UPDATE,
			FIXED_UPDATE,
			SLOW_UPDATE
		}

		protected UCoroutine () { }

		private float interval = float.NaN;
		private float intervalTime = float.NaN;
		private bool intervalScaled;

		/// <summary>
		/// Create a suspended coroutine.
		/// </summary>
		/// <param name="coroutine">The coroutine to iterate through.</param>
		/// <param name="policy">The dispatch policy to adhere to.</param>
		/// <param name="interval">The time in seconds between each iteration.</param>
		/// <param name="scaled">Should the wait time be counted in scaled or unscaled time.</param>
		public UCoroutine ( Func<IEnumerable> coroutine, DispatchPolicy policy = DispatchPolicy.UPDATE, float interval = float.NaN, bool scaled = true )
		{
			this.coroutine = coroutine;

			this.policy = policy;

			if ( !float.IsNaN( interval ) )
			{
				SetInterval( interval, scaled );
			}

			Reset();
		}

		/// <summary>
		/// Create a suspended coroutine.
		/// </summary>
		/// <param name="action">The Action to wrap.</param>
		/// <param name="policy">The dispatch policy to adhere to.</param>
		/// <param name="interval">The time in seconds between each iteration.</param>
		/// <param name="scaled">Should the wait time be counted in scaled or unscaled time.</param>
		/// <param name="n">The number of times the coroutine should execute. Values less than one will result in an indefinite lifetime.</param>
		/// <param name="scaled">Should the wait time be counted in scaled or unscaled time.</param>
		public UCoroutine ( Action action, DispatchPolicy policy = DispatchPolicy.UPDATE, float interval = float.NaN, bool scaled = true, int n = 0 )
		{
			if ( n > 0 )
			{
				coroutine = () => AutoCoroutine( action, n );
			}
			else
			{
				coroutine = () => AutoCoroutine( action );
			}

			this.policy = policy;

			if ( !float.IsNaN( interval ) )
			{
				SetInterval( interval, scaled );
			}

			Reset();
		}

		/// <summary>
		/// Create and run a coroutine.
		/// </summary>
		/// <param name="coroutine">The coroutine to iterate through.</param>
		/// <param name="policy">The dispatch policy to adhere to.</param>
		/// <param name="interval">The time in seconds between each iteration.</param>
		/// <param name="delay">The time in seconds until the coroutine is first run.</param>
		/// <param name="scaled">Should the wait time be counted in scaled or unscaled time.</param>
		/// <returns>The new coroutine instance.</returns>
		public static UCoroutine Run( Func<IEnumerable> coroutine, DispatchPolicy policy = DispatchPolicy.UPDATE, float interval = float.NaN, float delay = float.NaN, bool scaled = true )
		{
			var instance = new UCoroutine( coroutine, policy, interval, scaled );

			if ( float.IsNaN( delay ) )
			{
				instance.Run();
			}
			else
			{
				instance.Suspend( delay, scaled );
			}

			return instance;
		}

		/// <summary>
		/// Create and run an automatically generated coroutine which will invoke an Action for every iteration of its lifetime.
		/// </summary>
		/// <param name="action">The Action to wrap.</param>
		/// <param name="policy">The dispatch policy to adhere to.</param>
		/// <param name="interval">The time in seconds between each iteration.</param>
		/// <param name="delay">The time in seconds until the coroutine is first run.</param>
		/// <param name="scaled">Should the wait time be counted in scaled or unscaled time.</param>
		/// <param name="n">The number of times the coroutine should execute. Values less than one will result in an indefinite lifetime.</param>
		/// <returns>The new coroutine instance.</returns>
		public static UCoroutine RunAuto( Action action, DispatchPolicy policy = DispatchPolicy.UPDATE, float interval = float.NaN, float delay = float.NaN, bool scaled = true, int n = 0 )
		{
			var instance = new UCoroutine( action, policy, interval, scaled, n );

			if ( float.IsNaN( delay ) )
			{
				instance.Run();
			}
			else
			{
				instance.Suspend( delay, scaled );
			}

			return instance;
		}

		/// <summary>
		/// Run this coroutine.
		/// </summary>
		public void Run ()
		{
			//if the coroutine has not yet been run

			if ( ! hasStarted )
			{
				hasStarted = true;
				isRunning = true;
				Register();
			}

			//otherwise, simply resume

			else if ( ! isRunning && ! isDone )
			{
				isRunning = true;
				Register();
			}
		}

		/// <summary>
		/// Set this coroutine's dispatch interval.
		/// </summary>
		/// <param name="interval">The time in seconds between each iteration.</param>
		/// <param name="scaled">Should the wait time be counted in scaled or unscaled time.</param>
		public void SetInterval( float interval, bool scaled )
		{
			if ( interval != this.interval || scaled != intervalScaled )
			{
				//ensure that the correct delegate is subtracted from the event

				if ( isRunning )
				{
					Unregister();
				}

				//initialize interval time

				if ( float.IsNaN( intervalTime ) )
				{
				}

				//apply changes

				if ( interval <= 0f )
				{
					this.interval = float.NaN;
					intervalTime = float.NaN;
				}
				else
				{
					this.interval = interval;
					intervalTime = (scaled ? Time.time : Time.unscaledTime) + this.interval;
				}

				intervalScaled = scaled;

				//re-add appropriate delegate to event

				if ( isRunning )
				{
					Register();
				}
			}
		}

		/// <summary>
		/// Set this coroutine's dispatch policy.
		/// </summary>
		/// <param name="policy">The dispatch policy to adhere to.</param>
		public void SetPolicy( DispatchPolicy policy )
		{
			if ( policy != this.policy )
			{
				//ensure that the correct delegate is subtracted from the event

				if ( isRunning )
				{
					Unregister();
				}
				else if ( condition != null )
				{
					Unregister( TestCondition );
				}
				else if ( float.IsNaN( suspendTime ) )
				{
					Unregister( TestTime );
				}

				//apply changes

				this.policy = policy;

				//re-add appropriate delegate to event

				if ( isRunning )
				{
					Register();
				}
				else if ( condition != null )
				{
					Register( TestCondition );
				}
				else if ( float.IsNaN( suspendTime ) )
				{
					Register( TestTime );
				}
			}
		}

		/// <summary>
		/// Suspend this coroutine.
		/// </summary>
		public void Halt ()
		{
			if ( isRunning )
			{
				isRunning = false;
				Unregister( Invoke );
			}
		}

		private float suspendTime = float.NaN;
		private bool suspendScaled;

		/// <summary>
		/// Suspend this coroutine for some time.
		/// </summary>
		/// <param name="time">Seconds to wait before running this coroutine.</param>
		/// <param name="scaled">Should the wait time be counted in scaled or unscaled time.</param>
		public void Suspend ( float time, bool scaled )
		{
			if ( time != this.suspendTime || scaled != this.suspendScaled )
			{
				//ensure that the correct delegate is subtracted from the event

				if ( isRunning )
				{
					isRunning = false;
					Unregister();
					Register( TestTime );
				}
				else if ( condition != null )
				{
					Unregister( TestCondition );
					Register( TestTime );
				}

				//setup suspension parameters

				suspendTime = (scaled ? Time.time : Time.unscaledTime) + time;
				suspendScaled = scaled;
			}
		}

		private Func<bool> condition;

		/// <summary>
		/// Suspend this coroutine until some condition is met.
		/// </summary>
		/// <param name="condition">Condition that must return true before this coroutine runs.</param>
		public void Suspend ( Func<bool> condition )
		{
			//ensure that the correct delegate is subtracted from the event

			if ( isRunning )
			{
				isRunning = false;
				Unregister();
				Register( TestCondition );
			}
			else if ( ! float.IsNaN( suspendTime ) )
			{
				Unregister( TestTime );
				Register( TestCondition );
			}

			//setup suspension parameters
			this.condition = condition;
		}

		/// <summary>
		/// Move this coroutine to its initial state.
		/// </summary>
		public void Reset ()
		{
			enumerator = coroutine().GetEnumerator();
		}

		private bool TestTime ()
		{
			//check if the suspend time has elapsed

			if ( (suspendScaled ? Time.time : Time.unscaledTime) >= suspendTime )
			{
				Unregister( TestTime );
				Register();
				isRunning = true;
				suspendTime = float.NaN;
				return true;
			}

			return false;
		}

		private bool TestCondition ()
		{
			//check if the suspend condition has been met

			if ( condition() )
			{
				Unregister( TestCondition );
				Register();
				isRunning = true;
				condition = null;
				return true;
			}

			return false;
		}

		private bool InvokeInterval ()
		{
			//check if the interval time has elapsed

			if ( (intervalScaled ? Time.time : Time.unscaledTime) >= intervalTime )
			{
				intervalTime += interval;
				return Invoke();
			}

			return true;
		}

		protected virtual bool Invoke ()
		{
			//perform one iteration, if the enumeration has been exhausted, clean up

			if ( ! enumerator.MoveNext() )
			{
				isDone = true;
				isRunning = false;

				Unregister();

				if ( OnDone != null )
				{
					OnDone( this );
				}

				return false;
			}

			return true;
		}

		private void Register ()
		{
			//register either the lightweight invoke (no custom wait time) or the more expensive invoke (with custom wait time)
			//depending on whether or not an interval has been specified

			if ( float.IsNaN( interval ) )
			{
				Register( Invoke );
			}
			else
			{
				Register( InvokeInterval );
			}
		}

		private void Unregister ()
		{
			//complements the behaviour in Register ()

			if ( float.IsNaN( interval ) )
			{
				Unregister( Invoke );
			}
			else
			{
				Unregister( InvokeInterval );
			}
		}

		private void Register ( Func<bool> handler )
		{
			switch ( policy )
			{
			case DispatchPolicy.UPDATE:
				Update += handler;
				break;

			case DispatchPolicy.LATE_UPDATE:
				LateUpdate += handler;
				break;

			case DispatchPolicy.FIXED_UPDATE:
				FixedUpdate += handler;
				break;

			case DispatchPolicy.SLOW_UPDATE:
				SlowUpdate += handler;
				break;
			}
		}

		private void Unregister ( Func<bool> handler )
		{
			switch ( policy )
			{
			case DispatchPolicy.UPDATE:
				Update -= handler;
				break;

			case DispatchPolicy.LATE_UPDATE:
				LateUpdate -= handler;
				break;

			case DispatchPolicy.FIXED_UPDATE:
				FixedUpdate -= handler;
				break;

			case DispatchPolicy.SLOW_UPDATE:
				SlowUpdate -= handler;
				break;
			}
		}
	}
}
