﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ToBoldlyPlay.Functional
{
	public static class Series
	{
		public static int[] Indices ( int n )
		{
			int[] arr = new int[n];

			for ( int i = 0; i < n; i++ )
			{
				arr[i] = i;
			}

			return arr;
		}
		
		public static int[] Indices<T> ( this T[] array )
		{
			return Indices( array.Length );
		}
		
		public static int[] Indices ( this IList list )
		{
			return Indices( list.Count );
		}
	}
}
