﻿using System;
using System.Collections;

namespace ToBoldlyPlay.Threading
{
	public abstract class Job : IRun
	{
		private Action callback;

		private bool hasStarted = false;

		private bool isDone = false;

		private object hasStartedMutex = new object();

		private object hasFinishedMutex = new object();

		private System.Threading.Thread thread = null;

		/// <summary>
		/// Query whether or not this Job has been started. (thread-safe)
		/// </summary>
		public bool HasStarted
		{
			get
			{
				return SafeHasStarted;
			}
		}

		private bool SafeHasStarted
		{
			get
			{
				bool value;

				lock ( hasStartedMutex )
				{
					value = hasStarted;
				}

				return value;
			}
			set
			{
				lock ( hasStartedMutex )
				{
					hasStarted = value;
				}
			}
		}
		
		/// <summary>
		/// Query whether or not this Job is done. (thread-safe)
		/// </summary>
		public bool IsDone
		{
			get
			{
				return SafeIsDone;
			}
		}

		private bool SafeIsDone
		{
			get
			{
				bool value;

				lock ( hasFinishedMutex )
				{
					value = isDone;
				}

				return value;
			}
			set
			{
				lock ( hasFinishedMutex )
				{
					isDone = value;
				}
			}
		}

		/// <summary>
		/// Creates and starts a thread to execute the implemented process. (thread-safe)
		/// </summary>
		public virtual void Run ()
		{
			if ( !SafeHasStarted )
			{
				PreProcess();

				thread = new System.Threading.Thread( ComputationWrapper );
				thread.Start();

				refreshCoroutine = UCoroutine.Run( RefreshCoroutine );
			}
			else
			{
				Abort();

				Run();
			}
		}

		public void Run ( Action callback )
		{
			this.callback = callback;

			Run();
		}

		private UCoroutine refreshCoroutine;

        public event Action<IRun> OnDone;

        /// <summary>
        /// Aborts the currently executing thread. (thread-safe)
        /// </summary>
        public virtual void Abort ()
		{
			if ( SafeHasStarted )
			{
				thread.Abort();
				thread = null;

				SafeIsDone = false;
				SafeHasStarted = false;
			}
		}

		/// <summary>
		/// Override this method for setup actions. Executed in main thread.
		/// </summary>
		protected virtual void PreProcess () { }

		/// <summary>
		/// Implement this method for primary work. Executed in job thread.
		/// </summary>
		protected abstract void Process ();

		/// <summary>
		/// Override this for method finishing actions. Executed in main thread.
		/// </summary>
		protected virtual void PostProcess () { }

		/// <summary>
		/// Poll whether or not this job is done. (thread-safe)
		/// </summary>
		/// <returns>True, if the job has finished.</returns>
		public virtual bool Poll ()
		{
			if ( SafeIsDone )
			{
				PostProcess();

				SafeHasStarted = false;
				SafeIsDone = false;

				refreshCoroutine.Halt();

				return true;
			}

			return false;
		}

		/// <summary>
		/// Coroutine to automate the calling of Refresh each frame.
		/// </summary>
		private IEnumerable RefreshCoroutine ()
		{
			while ( !Poll() && SafeHasStarted )
			{
				yield return null;
			}

			if ( callback != null )
			{
				callback.Invoke();
			}
		}

		private void ComputationWrapper ()
		{
			SafeHasStarted = true;

			Process();

			SafeIsDone = true;
		}

        public void Halt ()
        {
            throw new NotImplementedException();
        }

        public void Reset ()
        {
            throw new NotImplementedException();
        }
    }
}
