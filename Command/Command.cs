﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ToBoldlyPlay.Patterns
{
	/// <summary>
	/// 
	/// </summary>
	public abstract class Command : IRun, IExecute
	{
		private UCoroutine coroutine;

		public event Action<IRun> OnDone;

		/// <summary>
		/// 
		/// </summary>
		public bool IsDone
		{
			get
			{
				return coroutine.IsDone;
			}
		}

		public bool IsRunning
		{
			get
			{
				return coroutine.IsRunning;
			}
		}

		/// <summary>
		/// Default constructor.
		/// </summary>
		public Command ()
		{
			coroutine = new UCoroutine( Coroutine );
		}

		/// <summary>
		/// Execute this command.
		/// </summary>
		public abstract void Execute ();

		/// <summary>
		/// Override this method to implement commands that are executed over some nonzero time interval / comprise subcommands.
		/// </summary>
		/// <returns>False, if the coroutine has terminated.</returns>
		protected virtual IEnumerable Coroutine ()
		{
			if ( OnDone != null )
			{
				OnDone( this );
			}

			yield break;
		}

		/// <summary>
		/// 
		/// </summary>
		public void Reset ()
		{
			coroutine.Reset();
		}

		/// <summary>
		/// 
		/// </summary>
		public void Run ()
		{
			coroutine.Run();
		}

		/// <summary>
		/// Suspend this coroutine.
		/// </summary>
		public void Halt ()
		{
			coroutine.Halt();
		}

		/// <summary>
		/// Suspend this coroutine for some time.
		/// </summary>
		/// <param name="time">Seconds to wait before running this coroutine.</param>
		/// <param name="scaled">Should the wait time be counted in scaled or unscaled time.</param>
		public void Suspend ( float time, bool scaled )
		{
			coroutine.Suspend( time, scaled );
		}

		/// <summary>
		/// Suspend this coroutine until some condition is met.
		/// </summary>
		/// <param name="condition">Condition that must return true before this coroutine runs.</param>
		public void Suspend ( Func<bool> condition )
		{
			coroutine.Suspend( condition );
		}
	}

	/// <summary>
	/// 
	/// </summary>
	public abstract class UndoableCommand : Command, IUndo
	{
		/// <summary>
		/// 
		/// </summary>
		public abstract void Undo ();
	}
}
