﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace ToBoldlyPlay.Patterns
{
	/// <summary>
	/// Base class for all executables.
	/// </summary>
	public class Executable : IExecute
	{
		/// <summary>
		/// 
		/// </summary>
		public readonly Action action;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="action"></param>
		public Executable ( System.Action action ) { this.action = action; }
		
		/// <summary>
		/// Execute this command.
		/// </summary>
		public void Execute () { action(); }
	}

	/// <summary>
	/// Base class for all targeted executables.
	/// </summary>
	/// <typeparam name="T">Target type.</typeparam>
	public class Executable<T> : IExecute
	{
		/// <summary>
		/// 
		/// </summary>
		public readonly System.Action<IEnumerable<T>> action;

		/// <summary>
		/// Read only collection of the elements targeted by this executer.
		/// </summary>
		public readonly ReadOnlyCollection<T> targets;

		/// <summary>
		/// Create a new executable.
		/// </summary>
		/// <param name="action">The action to perform on the collection of targets.</param>
		/// <param name="targets">One or more targets.</param>
		public Executable ( System.Action<IEnumerable<T>> action, params T[] targets )
		{
			this.action = action;
			this.targets = new ReadOnlyCollection<T>( targets );
		}
		
		/// <summary>
		/// Execute this command.
		/// </summary>
		public void Execute () { action( targets ); }
	}

	/// <summary>
	/// Extends Executable to facilitate iterative execution over a collection of targets.
	/// </summary>
	/// <typeparam name="T">Target type.</typeparam>
	public abstract class IterativeExecutable<T> : IExecute
	{
		/// <summary>
		/// 
		/// </summary>
		public readonly System.Action<T> action;

		/// <summary>
		/// Read only collection of the elements targeted by this executer.
		/// </summary>
		public readonly ReadOnlyCollection<T> targets;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="action">The action to perform on each target in the collection.</param>
		/// <param name="targets">One or more targets.</param>
		public IterativeExecutable ( System.Action<T> action, params T[] targets )
		{
			this.action = action;
			this.targets = new ReadOnlyCollection<T>( targets );
		}

		/// <summary>
		/// Execute this command.
		/// </summary>
		public void Execute ()
		{
			for ( int i = 0; i < targets.Count; i++ )
			{
				Execute( targets[i] );
			}
		}

		/// <summary>
		/// Called for each target in this command's target collection.
		/// </summary>
		/// <param name="target">The target to execute on.</param>
		protected void Execute ( T target ) { action( target ); }
	}
}