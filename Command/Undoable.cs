﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ToBoldlyPlay.Patterns
{
	/// <summary>
	/// Base class for all undoables.
	/// </summary>
	public class Undoable : IUndo
	{
		/// <summary>
		/// 
		/// </summary>
		public readonly System.Action action;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="action"></param>
		public Undoable ( System.Action action ) { this.action = action; }
		
		/// <summary>
		/// Undo this command.
		/// </summary>
		public void Undo () { action(); }
	}

	/// <summary>
	/// Base class for all targeted undoables.
	/// </summary>
	/// <typeparam name="T">Target type.</typeparam>
	public class Undoable<T> : IUndo
	{
		/// <summary>
		/// 
		/// </summary>
		public readonly System.Action<IEnumerable<T>> action;

		/// <summary>
		/// Read only collection of the elements targeted by this undor.
		/// </summary>
		public readonly ReadOnlyCollection<T> targets;

		/// <summary>
		/// Create a new undoable.
		/// </summary>
		/// <param name="action">The action to perform on the collection of targets.</param>
		/// <param name="targets">One or more targets.</param>
		public Undoable ( System.Action<IEnumerable<T>> action, params T[] targets )
		{
			this.action = action;
			this.targets = new ReadOnlyCollection<T>( targets );
		}
		
		/// <summary>
		/// Undo this command.
		/// </summary>
		public void Undo () { action( targets ); }
	}

	/// <summary>
	/// Extends Undoable to facilitate iterative execution over a collection of targets.
	/// </summary>
	/// <typeparam name="T">Target type.</typeparam>
	public abstract class IterativeUndoable<T> : IUndo
	{
		/// <summary>
		/// 
		/// </summary>
		public readonly System.Action<T> action;

		/// <summary>
		/// Read only collection of the elements targeted by this undor.
		/// </summary>
		public readonly ReadOnlyCollection<T> targets;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="action">The action to perform on each target in the collection.</param>
		/// <param name="targets">One or more targets.</param>
		public IterativeUndoable ( System.Action<T> action, params T[] targets )
		{
			this.action = action;
			this.targets = new ReadOnlyCollection<T>( targets );
		}

		/// <summary>
		/// Undo this command.
		/// </summary>
		public void Undo ()
		{
			for ( int i = 0; i < targets.Count; i++ )
			{
				Undo( targets[i] );
			}
		}

		/// <summary>
		/// Called for each target in this command's target collection.
		/// </summary>
		/// <param name="target">The target to undo on.</param>
		protected void Undo ( T target ) { action( target ); }
	}
}