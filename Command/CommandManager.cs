﻿using System;
using System.Collections.Generic;

namespace ToBoldlyPlay.Patterns
{
	/// <summary>
	/// A class that manages types implementing the ICommand and IUndoRedoCommand interfaces. Holds a stack of commands that can be undone or redone.
	/// </summary>
	public partial class CommandManager
	{
		private Stack<CommandStack> contextUndoStack;
		private Stack<CommandStack> contextRedoStack;

		private CommandStack context;

		/// <summary>
		/// The runnable currently executing.
		/// </summary>
		public IRun Current { get { return context.coroutine; } }

		/// <summary>
		/// The number of runnables encapsulating the currently executing runnable.
		/// </summary>
		public int Depth { get { return contextUndoStack.Count; } }

		/// <summary>
		/// Create a new CommandManager.
		/// </summary>
		public CommandManager ()
		{
			contextUndoStack = new Stack<CommandStack>();
			contextRedoStack = new Stack<CommandStack>();

			context = new CommandStack( new MinCoroutine() );
		}

		/// <summary>
		/// Execute an instantaneous command, pushing it onto the undo stack, if it supports undo.
		/// </summary>
		/// <param name="command">Command to execute.</param>
		public void Execute ( IExecute executable )
		{
			executable.Execute();

			if ( executable is IUndo )
			{
				//clear execute stack to maintain consistency
				context.executeStack.Clear();

				//push executed command onto the undo stack
				context.undoStack.Push( executable as IUndo );
			}
		}

		/// <summary>
		/// Undo the most recently executed command.
		/// </summary>
		public void Undo ()
		{
			if ( context.undoStack.Count > 0 )
			{
				//get the most recent undoable command
				var undoable = context.undoStack.Pop();

				//undo it
				undoable.Undo();

				//push it onto the redo stack
				context.executeStack.Push( undoable as IExecute );
			}
		}

		/// <summary>
		/// Re-execute the most recently undone command.
		/// </summary>
		public void Redo ()
		{
			if ( context.executeStack.Count > 0 )
			{
				//get the next redoable command
				var executable = context.executeStack.Pop();

				//execute it
				executable.Execute();

				//push it onto the undo stack
				context.undoStack.Push( executable as IUndo );
			}
		}

		/// <summary>
		/// Start executing a new runnable command.
		/// </summary>
		/// <param name="command">Command to execute.</param>
		public void Run<R> ( R command ) where R : ICoroutine, IExecute, IUndo
		{
			//store current context
			contextUndoStack.Push( context );

			//create a new context for the new runnable
			context = new CommandStack( command );

			//react to completion
			command.OnDone += HandleDone;

			//monitor and execute
			command.Run();
		}

		/// <summary>
		/// Abort of the top-most runnable. Cannot be undone.
		/// </summary>
		public void Abort ()
		{
			if ( contextUndoStack.Count > 0 )
			{
				//undo changes
				Rollback();

				//halt execution
				context.coroutine.Halt();

				//move up to encapsulating context and resume execution of its runnable
				context = contextUndoStack.Pop();
				context.coroutine.Run();
			}
		}

		/// <summary>
		/// Undo all changes under the currently executing runnable and restart it.
		/// </summary>
		private void Rollback ()
		{
			//undo all changes made to model since onset of runnable

			while ( context.undoStack.Count > 0 )
			{
				Undo();
			}

			//reset runnable
			context.coroutine.Reset();
		}

		/// <summary>
		/// Clear the undo and redo stacks, revert and discard any executing runnables. Cannot be undone.
		/// </summary>
		public void Clear ()
		{
			//halt all executing runnables, empty the context undo stack

			while ( contextUndoStack.Count > 0 )
			{
				//halt execution
				context.coroutine.Halt();

				//move up to enclosing context
				context = contextUndoStack.Pop();
			}

			//empty the context redo stack
			contextRedoStack.Clear();
		}

		private void HandleDone ( IDoneEvent sender )
		{
			if ( sender.IsDone )
			{
				//collapse runnable's context
				context = contextUndoStack.Pop();

				//execute the runnable, allowing it to be undone / redone as an instantaneous command
				Execute( sender as IExecute );

				//resume execution of the enclosing context's runnable
				context.coroutine.Run();
			}
        }

        /// <summary>
        /// Numer of commands stored on the undo stack.
        /// </summary>
        public int UndoStackCount { get { return context.undoStack.Count; } }

        /// <summary>
        /// Number of commands stored on the redo stack.
        /// </summary>
        public int RedoStackCount { get { return context.executeStack.Count; } }

        /// <summary>
        /// Skeleton placeholder class for when no runnable context has been established, or all prior runnables have been exhausted.
        /// </summary>
        private class MinCoroutine : ICoroutine
        {
            public event Action<IDoneEvent> OnDone;

            public bool IsDone
            {
                get
                {
                    return false;
                }
            }

            public void Reset () { }

            public void Run () { }

            public void Halt () { }
        }
    }
}