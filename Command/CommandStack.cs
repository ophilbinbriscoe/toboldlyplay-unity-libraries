﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToBoldlyPlay.Patterns
{
    /// <summary>
    /// Holds undo / execute stacks and references runnable.
    /// </summary>
    public class CommandStack
    {
        public readonly Stack<IUndo> undoStack;
        public readonly Stack<IExecute> executeStack;

        public readonly ICoroutine coroutine;

        public CommandStack ( ICoroutine coroutine )
        {
            undoStack = new Stack<IUndo>();
            executeStack = new Stack<IExecute>();

            this.coroutine = coroutine;
        }
    }
}