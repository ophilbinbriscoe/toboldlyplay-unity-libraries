﻿using System;
using System.Collections;
using UnityEngine;

namespace ToBoldlyPlay.ObjectOriented
{
    /// <summary>
    /// Extends MonoBehaviour, restoring C# override functionality to default MonoBehaviour messages.
    /// Simplifies the Destroy process (simply call Destroy() to have the appropriate destruction procedure executed).
    /// Implements shorthand for creating and running a UCoroutine.
    /// </summary>
    [Serializable]
	public class UBehaviour : MonoBehaviour
	{
		/// <summary>
		/// Destroy this UBehaviour.
		/// </summary>
		public void Destroy ()
		{
			if ( Application.isEditor && !Application.isPlaying )
			{
				DestroyImmediate( this, true );
			}
			else
			{
				Destroy( this );
			}
		}

		/// <summary>
		/// Run a coroutine.
		/// </summary>
		/// <param name="delay">The time to wait before running.</param>
		/// <param name="scaled">Whether to count time in scaled or unscaled time.</param>
		/// <param name="coroutine">The coroutine to iterate through.</param>
		/// <param name="policy">The dispatch policy to adhere to.</param>
		/// <param name="interval">The time in seconds between iterations.</param>
		/// <returns>The new coroutine instance.</returns>
		public UCoroutine Run ( Func<IEnumerable> coroutine, UCoroutine.DispatchPolicy policy = UCoroutine.DispatchPolicy.UPDATE, float interval = float.NaN, float delay = float.NaN, bool scaled = true )
		{
			return UCoroutine.Run( coroutine, policy, interval, delay, scaled );
		}

		/// <summary>
		/// Run an automatically generated coroutine which will invoke an Action for every iteration of its lifetime.
		/// </summary>
		/// <param name="action">The Action to wrap.</param>
		/// <param name="policy">The dispatch policy to adhere to.</param>
		/// <param name="interval">The time in seconds between each iteration.</param>
		/// <param name="delay">The time in seconds until the coroutine is first run.</param>
		/// <param name="scaled">Should the wait time be counted in scaled or unscaled time.</param>
		/// <param name="n">The number of times the coroutine should execute. Values less than one will result in an indefinite lifetime.</param>
		/// <returns>The new coroutine instance.</returns>
		public UCoroutine RunAuto ( Action action, UCoroutine.DispatchPolicy policy = UCoroutine.DispatchPolicy.UPDATE, float interval = float.NaN, float delay = float.NaN, bool scaled = true, int n = 0 )
		{
			return UCoroutine.RunAuto( action, policy, interval, delay, scaled, n );
		}

		void Awake ()
		{
			FAwake();
		}

		void Start ()
		{
			FStart();
		}

		void OnDestroy ()
		{
			FOnDestroy();
		}

		void OnEnable ()
		{
			FOnEnable();
		}

		void OnDisable ()
		{
			FOnDisable();
		}

		void Update ()
		{
			FUpdate();
		}

		void LateUpdate ()
		{
			FLateUpdate();
		}

		void FixedUpdate ()
		{
			FFixedUpdate();
		}

		void OnPreRender ()
		{
			FOnPreRender();
		}

		void OnPostRender ()
		{
			FOnPostRender();
		}

		void OnPreCull ()
		{
			FOnPreCull();
		}

		void OnRenderImage ( RenderTexture source, RenderTexture destination )
		{
			FOnRenderImage( source, destination );
		}

		void OnRenderObject ()
		{
			FOnRenderObject();
		}

		void OnWillRenderObject ()
		{
			FOnWillRenderObject();
		}

		void OnConnectedToServer ()
		{
			FOnConnectedToServer();
		}

		void OnDisconnectedFromServer ( NetworkDisconnection info )
		{
			FOnDisconnectedFromServer( info );
		}

		void OnPlayerConnected ( NetworkPlayer player )
		{
			FOnPlayerConnected( player );
		}

		void OnPlayerDisconnected ( NetworkPlayer player )
		{
			FOnPlayerDisconnected( player );
		}

		void OnNetworkInstantiate ( NetworkMessageInfo info )
		{
			FOnNetworkInstantiate( info );
		}

		void OnFailedToConnect ( NetworkConnectionError error )
		{
			FOnFailedToConnect( error );
		}

		void OnFailedToConnectToMasterServer ( NetworkConnectionError error )
		{
			FOnFailedToConnectToMasterServer( error );
		}

		void OnMasterServerEvent ( MasterServerEvent masterServerEvent )
		{
			FOnMasterServerEvent( masterServerEvent );
		}

		void OnSerializeNetworkView ( BitStream stream, NetworkMessageInfo info )
		{
			FOnSerializeNetworkView( stream, info );
		}

		void OnCollisionEnter ( Collision collision )
		{
			FOnCollisionEnter( collision );
		}

		void OnCollisionEnter2D ( Collision2D collision )
		{
			FOnCollisionEnter2D( collision );
		}

		void OnCollisionExit ( Collision collision )
		{
			FOnCollisionExit( collision );
		}

		void OnCollisionExit2D ( Collision2D collision )
		{
			FOnCollisionExit2D( collision );
		}

		void OnCollisionStay ( Collision collision )
		{
			FOnCollisionStay( collision );
		}

		void OnCollisionStay2D ( Collision2D collision )
		{
			FOnCollisionStay2D( collision );
		}

		void OnTriggerEnter ( Collider collider )
		{
			FOnTriggerEnter( collider );
		}

		void OnTriggerEnter2D ( Collider2D collider )
		{
			FOnTriggerEnter2D( collider );
		}

		void OnTriggerExit ( Collider collider )
		{
			FOnTriggerExit( collider );
		}

		void OnTriggerExit2D ( Collider2D collider )
		{
			FOnTriggerExit2D( collider );
		}

		void OnTriggerStay ( Collider collider )
		{
			FOnTriggerStay( collider );
		}

		void OnTriggerStay2D ( Collider2D collider )
		{
			FOnTriggerStay2D( collider );
		}

		void OnParticleTrigger ()
		{
			FOnParticleTrigger();
		}

		void OnJointBreak ( float breakForce )
		{
			FOnJointBreak( breakForce );
		}

		void OnGUI ()
		{
			FOnGUI();
		}

		void OnDrawGizmos ()
		{
			FOnDrawGizmos();
		}

		void OnDrawGizmosSelected ()
		{
			FOnDrawGizmosSelected();
		}

		void OnLevelWasLoaded ( int level )
		{
			FOnLevelWasLoaded( level );
		}

		void OnApplicationFocus ( bool focusStatus  )
		{
			FOnApplicationFocus( focusStatus );
		}

		void OnApplicationPause ( bool pauseStatus )
		{
			FOnApplicationPause( pauseStatus );
		}

		void OnApplicationQuit ()
		{
			FOnApplicationQuit();
		}

		void OnValidate ()
		{
			FOnValidate();
		}

		protected virtual void FAwake ()
		{

		}

		protected virtual void FStart ()
		{

		}

		protected virtual void FOnDestroy ()
		{

		}

		protected virtual void FOnEnable ()
		{

		}

		protected virtual void FOnDisable ()
		{

		}

		protected virtual void FUpdate ()
		{

		}

		protected virtual void FLateUpdate ()
		{

		}

		protected virtual void FFixedUpdate ()
		{

		}

		protected virtual void FOnPreRender ()
		{

		}

		protected virtual void FOnPostRender ()
		{

		}

		protected virtual void FOnPreCull ()
		{

		}

		protected virtual void FOnRenderImage ( RenderTexture source, RenderTexture destination )
		{

		}

		protected virtual void FOnRenderObject ()
		{

		}

		protected virtual void FOnWillRenderObject ()
		{

		}

		protected virtual void FOnConnectedToServer ()
		{

		}

		protected virtual void FOnDisconnectedFromServer ( NetworkDisconnection info )
		{

		}

		protected virtual void FOnPlayerConnected ( NetworkPlayer player )
		{

		}

		protected virtual void FOnPlayerDisconnected ( NetworkPlayer player )
		{

		}

		protected virtual void FOnNetworkInstantiate ( NetworkMessageInfo info )
		{

		}

		protected virtual void FOnFailedToConnect ( NetworkConnectionError error )
		{

		}

		protected virtual void FOnFailedToConnectToMasterServer ( NetworkConnectionError error )
		{

		}

		protected virtual void FOnMasterServerEvent ( MasterServerEvent masterServerEvent )
		{

		}

		protected virtual void FOnSerializeNetworkView ( BitStream stream, NetworkMessageInfo info )
		{

		}

		protected virtual void FOnCollisionEnter ( Collision collision )
		{

		}

		protected virtual void FOnCollisionEnter2D ( Collision2D collision )
		{

		}

		protected virtual void FOnCollisionExit ( Collision collision )
		{

		}

		protected virtual void FOnCollisionExit2D ( Collision2D collision )
		{

		}

		protected virtual void FOnCollisionStay ( Collision collision )
		{

		}

		protected virtual void FOnCollisionStay2D ( Collision2D collision )
		{

		}

		protected virtual void FOnTriggerEnter ( Collider collider )
		{

		}

		protected virtual void FOnTriggerEnter2D ( Collider2D collider )
		{

		}

		protected virtual void FOnTriggerExit ( Collider collider )
		{

		}

		protected virtual void FOnTriggerExit2D ( Collider2D collider )
		{

		}

		protected virtual void FOnTriggerStay ( Collider collider )
		{

		}

		protected virtual void FOnTriggerStay2D ( Collider2D collider )
		{

		}

		protected virtual void FOnParticleTrigger ()
		{

		}
		
		protected virtual void FOnJointBreak ( float breakForce )
		{

		}

		protected virtual void FOnGUI ()
		{

		}

		protected virtual void FOnDrawGizmos ()
		{

		}

		protected virtual void FOnDrawGizmosSelected ()
		{

		}

		protected virtual void FOnLevelWasLoaded ( int level )
		{

		}

		protected virtual void FOnApplicationFocus ( bool pauseStatus )
		{

		}

		protected virtual void FOnApplicationPause ( bool pauseStatus )
		{

		}

		protected virtual void FOnApplicationQuit ()
		{

		}

		protected virtual void FOnValidate ()
		{

		}
	}
}
