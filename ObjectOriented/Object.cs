﻿using System;
using System.Collections;
using UnityEngine;

namespace ToBoldlyPlay.ObjectOriented
{
	/// <summary>
	/// Extends MonoBehaviour, restoring C# override functionality to default MonoBehaviour messages.
	/// Simplifies the Destroy process (simply call Destroy() to have the appropriate destruction procedure executed).
	/// Implements shorthand for creating and running a UCoroutine.
	/// </summary>
	[Serializable]
	public class UObject : ScriptableObject
	{
		/// <summary>
		/// Destroy this UBehaviour.
		/// </summary>
		public void Destroy ()
		{
			if ( Application.isEditor && !Application.isPlaying )
			{
				DestroyImmediate( this, true );
			}
			else
			{
				Destroy( this );
			}
		}

		/// <summary>
		/// Run a coroutine.
		/// </summary>
		/// <param name="delay">The time to wait before running.</param>
		/// <param name="scaled">Whether to count time in scaled or unscaled time.</param>
		/// <param name="coroutine">The coroutine to iterate through.</param>
		/// <param name="policy">The dispatch policy to adhere to.</param>
		/// <param name="interval">The time in seconds between iterations.</param>
		/// <returns>The new coroutine instance.</returns>
		public UCoroutine Run ( Func<IEnumerable> coroutine, UCoroutine.DispatchPolicy policy = UCoroutine.DispatchPolicy.UPDATE, float interval = float.NaN, float delay = float.NaN, bool scaled = true )
		{
			return UCoroutine.Run( coroutine, policy, interval, delay, scaled );
		}

		/// <summary>
		/// Run an automatically generated coroutine which will invoke an Action for every iteration of its lifetime.
		/// </summary>
		/// <param name="action">The Action to wrap.</param>
		/// <param name="policy">The dispatch policy to adhere to.</param>
		/// <param name="interval">The time in seconds between each iteration.</param>
		/// <param name="delay">The time in seconds until the coroutine is first run.</param>
		/// <param name="scaled">Should the wait time be counted in scaled or unscaled time.</param>
		/// <param name="n">The number of times the coroutine should execute. Values less than one will result in an indefinite lifetime.</param>
		/// <returns>The new coroutine instance.</returns>
		public UCoroutine RunAuto ( Action action, UCoroutine.DispatchPolicy policy = UCoroutine.DispatchPolicy.UPDATE, float interval = float.NaN, float delay = float.NaN, bool scaled = true, int n = 0 )
		{
			return UCoroutine.RunAuto( action, policy, interval, delay, scaled, n );
		}

		void OnDestroy ()
		{
			FOnDestroy();
		}

		void OnEnable ()
		{
			FOnEnable();
		}

		void OnDisable ()
		{
			FOnDisable();
		}

		protected virtual void FOnDestroy ()
		{

		}

		protected virtual void FOnEnable ()
		{

		}

		protected virtual void FOnDisable ()
		{

		}
	}
}
