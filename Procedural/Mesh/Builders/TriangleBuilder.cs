﻿using System.Collections.Generic;
using UnityEngine;
using ToBoldlyPlay.Geometric;

namespace ToBoldlyPlay.Procedural
{
	public class TriangleBuilder
	{
		protected readonly MeshStream stream;

		public TriangleBuilder ( MeshStream stream )
		{
			this.stream = stream;
		}

		public void AppendTriangle( Vector3[] vertices )
		{
			AppendTriangle( vertices[0], vertices[1], vertices[2] );
		}

		public void AppendTriangle( Triangle<Vector3> triangle )
		{
			AppendTriangle( triangle.a, triangle.b, triangle.c );
		}

		public void AppendTriangle( Vector3 a, Vector3 b, Vector3 c )
		{
			stream.Triangle
				(
				stream.Vertex( a ).Index(),
				stream.Vertex( b ).Index(),
				stream.Vertex( c ).Index()
				);
		}
	}
}
