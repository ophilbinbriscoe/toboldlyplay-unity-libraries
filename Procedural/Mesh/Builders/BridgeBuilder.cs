﻿using System.Collections.Generic;
using UnityEngine;

namespace ToBoldlyPlay.Procedural
{
	public class BridgeBuilder
	{
		protected readonly TriangleBuilder builder;
		
		public BridgeBuilder ( TriangleBuilder builder )
		{
			this.builder = builder;
		}

		public void AppendBridge( Vector3[] from, Vector3[] to )
		{			
//			Debug.Draw ( from, Color.red );
//			Debug.Draw ( to, Color.blue );
			
			IList<Vector3> fromPoints = from;
			IList<Vector3> toPoints = to;
			
			int f = 0;
			int t = 0;

						
			Vector3[] v = new Vector3[3];

					
			while( true ) {
				
				if ( Vector3.Distance( fromPoints[f], toPoints[t + 1] ) <= Vector3.Distance( fromPoints[t], toPoints[f + 1] ) ) {
				
					v[2] = fromPoints[f];
					v[1] = toPoints[t + 1];
					v[0] = toPoints[t];
					t++;
					
				} else {
				
					v[2] = fromPoints[f];
					v[1] = fromPoints[f + 1];
					v[0] = toPoints[t];
					f++;
					
				}
				
				builder.AppendTriangle( v );
				
				if ( f == fromPoints.Count - 1 )
				{
					
					v[2] = fromPoints[f];
					v[1] = toPoints[t + 1];
					v[0] = toPoints[t];
					
				}
				else if ( t == toPoints.Count - 1 )
				{
					v[2] = fromPoints[f];
					v[1] = fromPoints[f + 1];
					v[0] = toPoints[t];
				}
				else { continue; }
				
				builder.AppendTriangle( v );
				
				break;
			}
		}
	}
}
