﻿using UnityEngine;
using ToBoldlyPlay.Functional;
using ToBoldlyPlay.Geometric;

namespace ToBoldlyPlay.Procedural
{
	public class PolygonBuilder
	{
		public Triangulation.Function function;

		protected readonly TriangleBuilder builder;

		public PolygonBuilder ( TriangleBuilder builder ) : this( builder, Triangulation.Naive ) { }

		public PolygonBuilder (TriangleBuilder builder, Triangulation.Function function )
		{
			this.builder = builder;
			this.function = function;
		}

		public void AppendPolygon ( Vector3[] points )
		{
			function( points, points.Indices(), ( a, b, c ) => { builder.AppendTriangle( points[a], points[b], points[c] ); return false; } );
		}
	}
}
