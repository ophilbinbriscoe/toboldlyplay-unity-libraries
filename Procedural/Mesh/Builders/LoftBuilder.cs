﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ToBoldlyPlay.Geometric;

namespace ToBoldlyPlay.Procedural
{
	public struct LoftData
	{
		public readonly Vector3[] LHS;
		public readonly Vector3[] RHS;
		public readonly Vector3[] startFace;
		public readonly Vector3[] endFace;

		public LoftData ( Vector3[] LHS, Vector3[] RHS, Vector3[] startFace, Vector3[] endFace )
		{
			this.LHS = LHS;
			this.RHS = RHS;
			this.startFace = startFace;
			this.endFace = endFace;
		}
	}

	public class LoftBuilder
	{
		/// <summary>
		/// Function returning a value to scale the loft profile by when provided with the profile's absolute (Vector3) and normalized (float) positions along the loft path.
		/// </summary>
		public Func<Vector3,float,float> scaleFunction;

		/// <summary>
		/// Whether or not cap polygons should be appended to the start and end of the loft.
		/// </summary>
		public bool closed = true;

		private LoftData loftData;
		
		/// <summary>
		/// Information about the most recently appended loft.
		/// </summary>
		public LoftData MetaData
		{
			get
			{
				return loftData;
			}
		}

		protected readonly PolygonBuilder polygonBuilder;
		protected readonly ExtrusionBuilder extrusionBuilder;
		protected readonly BridgeBuilder bridgeBuilder;

		public LoftBuilder ( PolygonBuilder polygonBuilder, ExtrusionBuilder extrusionBuilder, BridgeBuilder bridgeBuilder )
		{
			this.polygonBuilder = polygonBuilder;
			this.extrusionBuilder = extrusionBuilder;
			this.bridgeBuilder = bridgeBuilder;
		}

		/// <summary>
		/// Appends a loft of a profile along a path to this LoftBuilder's MeshStream using best-guess values for start plane, end plane, and up vector.
		/// </summary>
		/// <param name="profile">Profile.</param>
		/// <param name="path">Path.</param>
		public void AppendLoft ( Vector3[] profile, Vector3[] path )
		{
			if ( scaleFunction == null )
			{
				AppendLoftUnscaled( profile, path, path[0] - path[1], path[path.Length - 1] - path[path.Length - 2], Vector3.up );
			}
			else
			{
				AppendLoftScaled( profile, path, path[0] - path[1], path[path.Length - 1] - path[path.Length - 2], Vector3.up );
			}
		}

		/// <summary>
		/// Appends a loft of a profile along a path to this LoftBuilder's MeshStream using best-guess values for start and end planes.
		/// </summary>
		/// <param name="profile">Profile.</param>
		/// <param name="path">Path.</param>
		/// <param name="up">Up.</param>
		public void AppendLoft ( Vector3[] profile, Vector3[] path, Vector3 up )
		{
			if ( scaleFunction == null )
			{
				AppendLoftUnscaled( profile, path, path[0] - path[1], path[path.Length - 1] - path[path.Length - 2], up );
			}
			else
			{
				AppendLoftScaled( profile, path, path[0] - path[1], path[path.Length - 1] - path[path.Length - 2], up );
			}
		}

		/// <summary>
		/// Appends a loft of a profile along a path to this LoftBuilder's Meshstream.
		/// </summary>
		/// <param name="profile">Profile.</param>
		/// <param name="path">Path.</param>
		/// <param name="startPlane">Start plane.</param>
		/// <param name="endPlane">End plane.</param>
		/// <param name="up">Up.</param>
		public void AppendLoft ( Vector3[] profile, Vector3[] path, Vector3 startPlane, Vector3 endPlane, Vector3 up )
		{
			if ( scaleFunction == null )
			{
				AppendLoftUnscaled( profile, path, startPlane, endPlane, up );
			}
			else
			{
				AppendLoftScaled( profile, path, startPlane, endPlane, up );
			}
		}

		private void AppendLoftUnscaled ( Vector3[] profile, Vector3[] path, Vector3 startPlane, Vector3 endPlane, Vector3 up )
		{
			List<Vector3> check = new List<Vector3>( path );

			for ( int c = 0; c < check.Count - 1; c++ )
			{
				if ( check[c] == check[c + 1] )
				{
					check.RemoveAt( c );
					c--;
				}
			}

			if ( check.Count < 2 )
			{
				loftData = new LoftData();
				return;
			}

			path = check.ToArray();

			Vector3[] LHS = new Vector3[path.Length];
			Vector3[] RHS = new Vector3[path.Length];

			Vector3[] startFace;
			Vector3[] endFace;

			Vector3 reflection; //direction vector for the next next segment, (i+1)th and ith
			Vector3 projection; //direction vector for the next segment, (i-1)th and (i)th points
			Vector3 normal;

			reflection = (path[0] - path[1]).normalized;

			startFace = Geometry.Projection( path[0], -startPlane, profile.Rotate( Quaternion.LookRotation( -reflection, up ) ).Displace( path[1] ), reflection );

			endFace = startFace;

			LHS[0] = endFace[0];
			RHS[0] = endFace[endFace.Length - 1];

			int i;
			float f = 1.0f / path.Length;
			float s = f;

			for ( i = 1; i < path.Length - 1; i++ )
			{
				projection = -reflection;
				reflection = (path[i] - path[i + 1]).normalized;
				normal = projection - reflection;

				endFace = extrusionBuilder.AppendExtrusion( path[i], normal, endFace, projection );

				LHS[i] = endFace[0];
				RHS[i] = endFace[endFace.Length - 1];
			}

			projection = -reflection;

			endFace = extrusionBuilder.AppendExtrusion( path[i], endPlane, endFace, projection );

			if ( closed )
			{
				polygonBuilder.AppendPolygon( startFace );
				polygonBuilder.AppendPolygon( endFace.Invert() );
			}

			LHS[i] = endFace[0];
			RHS[i] = endFace[endFace.Length - 1];

			loftData = new LoftData( LHS, RHS, startFace, endFace );
		}

		private void AppendLoftScaled ( Vector3[] profile, Vector3[] path, Vector3 startPlane, Vector3 endPlane, Vector3 up )
		{
			List<Vector3> check = new List<Vector3>( path );

			for ( int c = 0; c < check.Count - 1; c++ )
			{
				if ( check[c] == check[c + 1] )
				{
					check.RemoveAt( c );
					c--;
				}
			}

			if ( check.Count < 2 )
			{
				loftData = new LoftData();
				return;
			}

			path = check.ToArray();

			Vector3[] LHS = new Vector3[path.Length];
			Vector3[] RHS = new Vector3[path.Length];

			List<Vector3[]> profiles = new List<Vector3[]>( path.Length );

			Vector3 reflection; //direction vector for the next next segment, (i+1)th and ith
			Vector3 projection; //direction vector for the next segment, (i-1)th and (i)th points
			Vector3 normal;

			reflection = (path[0] - path[1]).normalized;

			profile = Geometry.Projection( path[0], -startPlane, profile.Rotate( Quaternion.LookRotation( -reflection, up ) ).Displace( path[1] ), reflection );

			profiles.Add( profile );

			LHS[0] = profile[0];
			RHS[0] = profile[profile.Length - 1];

			int i;
			float f = 1.0f / path.Length;
			float s = f;

			for ( i = 1; i < path.Length - 1; i++ )
			{
				projection = -reflection;
				reflection = (path[i] - path[i + 1]).normalized;
				normal = projection - reflection;

				profiles.Add( Geometry.Projection( path[i], normal, profiles[i - 1], projection ) );

				LHS[i] = profiles[i][0];
				RHS[i] = profiles[i][profiles[i].Length - 1];
			}

			projection = -reflection;

			profiles.Add( Geometry.Projection( path[i], endPlane, profiles[profiles.Count - 1], projection ) );

			LHS[i] = profiles[profiles.Count - 1][0];
			RHS[i] = profiles[profiles.Count - 1][profiles[profiles.Count - 1].Length - 1];

			i = 0;

			for ( ; i < profiles.Count; i++ )
			{
				bridgeBuilder.AppendBridge( profiles[i - 1].Scale( ((i - 1) / (profiles.Count - 1.0f) ), path[i - 1] ), profiles[i].Scale( (i / (profiles.Count - 1.0f) ), path[i] ) );
			}

			if ( closed )
			{
				polygonBuilder.AppendPolygon( profiles[0].Scale( scaleFunction( path[0], 0.0f ), path[0] ) );
				polygonBuilder.AppendPolygon( profiles[profiles.Count - 1].Invert().Scale( scaleFunction( path[path.Length - 1], 1.0f ), path[path.Length - 1] ) );
			}

			loftData = new LoftData( LHS, RHS, profiles[0], profiles[profiles.Count - 1] );
		}
	}
}
