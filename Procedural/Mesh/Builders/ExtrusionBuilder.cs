﻿using UnityEngine;
using ToBoldlyPlay.Geometric;

namespace ToBoldlyPlay.Procedural
{
	public class ExtrusionBuilder
	{
		protected readonly TriangleBuilder builder;

		public ExtrusionBuilder ( TriangleBuilder builder )
		{
			this.builder = builder;
		}

		public Vector3[] AppendExtrusion( Vector3 planePoint, Vector3 planeNormal, Vector3[] path, Vector3 projection )
		{			
			Vector3[] extrusion = Geometry.Projection ( planePoint, planeNormal, path, projection );
			
			for ( int i = 0; i < path.Length - 1; i++ )
			{
				builder.AppendTriangle( new Vector3[] { path[i], extrusion[i], extrusion[i + 1] } );
				builder.AppendTriangle( new Vector3[] { path[i + 1], path[i], extrusion[i + 1] } );
			}
			
			return extrusion;
		}
	}
}
