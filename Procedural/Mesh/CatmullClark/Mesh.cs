﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace ToBoldlyPlay.Procedural.CatmullClark
{
	public partial class Surface
	{
		public void CalculateNormals ()
		{
			// accumulate normals from each face
			foreach ( var face in faces )
			{
				face.vertices[0].normal += Vector3.Cross( (face.vertices[face.vertices.Length - 1].position - face.vertices[0].position), (face.vertices[1].position - face.vertices[0].position) );

				for ( int i = 1; i < face.vertices.Length - 1; i++ )
				{
					face.vertices[i].normal += Vector3.Cross( (face.vertices[i - 1].position - face.vertices[i].position), (face.vertices[i + 1].position - face.vertices[i].position) );
				}

				face.vertices[face.vertices.Length - 1].normal += Vector3.Cross( (face.vertices[face.vertices.Length - 2].position - face.vertices[face.vertices.Length - 1].position), (face.vertices[0].position - face.vertices[face.vertices.Length - 1].position) );
			}

			// normalize the sum
			foreach ( var vertex in verts )
			{
				vertex.normal = vertex.normal.normalized;
			}
		}
		
		/// <summary>
		/// Interpret this Surface onto a Mesh.
		/// </summary>
		/// <param name="mesh">The Mesh to interpret to.</param>
		/// <param name="calculateNormals">Whether or not to calculate normals from triangle data.</param>
		/// <param name="calculateTangents">Whether or not to calculate tangents from normal and uv data.</param>
		/// <param name="calculateBounds">Whether or not to calculate the Mesh's bounds. (recommended)</param>
		/// <param name="clear">Whether or not the Mesh should be cleared. Should only set to false if the Surface's vertex count is identical to the existing vertex count.</param>
		public void CopyTo ( Mesh mesh, bool calculateNormals = true, bool calculateTangents = true, bool calculateBounds = true, bool clear = true )
		{
			if ( clear )
			{
				mesh.Clear();
			}

			if ( calculateNormals )
			{
				CalculateNormals();
			}

			// 1-to-1 correspondence
			Vector3[] vertices = new Vector3[verts.Count];
			Vector3[] normals = new Vector3[verts.Count];
			Vector4[] tangents = new Vector4[verts.Count];
			Color[] colors = new Color[verts.Count];
			Vector2[] uv = new Vector2[verts.Count];

			for ( int i = 0; i < verts.Count; i++ )
			{
				vertices[i] = verts[i].position;
				normals[i] = verts[i].normal;
				//tangents[i] = verts[i].tangent;
				colors[i] = verts[i].color;
				uv[i] = verts[i].uv;

				verts[i].order = i;
			}

			// (2 triangles / quad) x (3 indices / triangles) = 6
			int[] triangles = new int[faces.Count * 6];

			for ( int i = 0, j = 0; i < faces.Count; i++ )
			{
				triangles[j++] = faces[i].vertices[2].order;
				triangles[j++] = faces[i].vertices[1].order;
				triangles[j++] = faces[i].vertices[0].order;

				triangles[j++] = faces[i].vertices[3].order;
				triangles[j++] = faces[i].vertices[2].order;
				triangles[j++] = faces[i].vertices[0].order;
			}

			mesh.vertices = vertices;
			mesh.triangles = triangles;
			mesh.normals = normals;
			mesh.tangents = tangents;
			mesh.uv = uv;

			if ( calculateTangents )
			{
				mesh.RecalculateTangents();
			}

			if ( calculateBounds )
			{
				mesh.RecalculateBounds();
			}
		}
	}
}
