﻿using UnityEngine;

namespace ToBoldlyPlay.Procedural.CatmullClark
{
	public static class Subdivision
	{
		/// <summary>
		/// Create a new surface by performing one iteration of the Catmull-Clark subdivision algorithm on an input surface. UV coordinates and vertex colors are interpolated.
		/// </summary>
		/// <param name="surface">Input surface. All faces are assumed to have either 3 or 4 sides.</param>
		/// <returns>Output surface.</returns>
		public static Surface Subdivide ( Surface surface )
		{
			Surface subdivision = new Surface( surface );

			foreach ( var edge in surface.edges )
			{
				subdivision.verts.Add( edge.point = new Vert( (edge.center + (edge.faces[0].point.position + edge.faces[1].point.position) / 2.0f) / 2.0f, (edge.a.uv + edge.b.uv) / 2.0f, (edge.a.color + edge.b.color) / 2.0f ) );
			}

			foreach ( var vertex in surface.verts )
			{
				//TODO: cache m1,2,3 for n = 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, etc. as needed (limit informed by initial # sides of trunk)

				float m1 = (vertex.adjacencies.Count - 3.0f) / vertex.adjacencies.Count;
				float m2 = 1.0f / vertex.adjacencies.Count;
				float m3 = 2.0f / vertex.adjacencies.Count;

				Vector3 face_point_average = Vector3.zero;

				foreach ( var adjacency in vertex.adjacencies )
				{
					face_point_average += adjacency.face.point.position;
				}

				face_point_average /= vertex.adjacencies.Count;

				Vector3 edge_center_average = vertex.edge_center_sum / vertex.edges;

				subdivision.verts.Add( vertex.descendant = new Vert( m1 * vertex.position + m2 * face_point_average + m3 * edge_center_average, vertex.uv, vertex.color ) );
			}

			foreach ( var face in surface.faces )
			{
				subdivision.verts.Add( face.point );

				if ( face.vertices.Length == 3 )
				{
					new Face( subdivision, face.vertices[0].descendant, face.edges[0].point, face.point, face.edges[2].point );
					new Face( subdivision, face.vertices[1].descendant, face.edges[1].point, face.point, face.edges[0].point );
					new Face( subdivision, face.vertices[2].descendant, face.edges[2].point, face.point, face.edges[1].point );
				}

				else // assume quad
				{
					new Face( subdivision, face.vertices[0].descendant, face.edges[0].point, face.point, face.edges[3].point );
					new Face( subdivision, face.vertices[1].descendant, face.edges[1].point, face.point, face.edges[0].point );
					new Face( subdivision, face.vertices[2].descendant, face.edges[2].point, face.point, face.edges[1].point );
					new Face( subdivision, face.vertices[3].descendant, face.edges[3].point, face.point, face.edges[2].point );
				}
			}

			return subdivision;
		}
	}
}
