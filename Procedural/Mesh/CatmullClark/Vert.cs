﻿using System.Collections.Generic;
using UnityEngine;

namespace ToBoldlyPlay.Procedural.CatmullClark
{
	public class Vert
	{
		// provided at construction
		public Vector3 position;

		// optional data members
		public Vector3 normal;
		public Vector2 uv;
		//public Vector4 tangent;
		public Color color;

		// directed edges, appened to in Face constructor
		public List<Adjacency> adjacencies;

		// incremented in Edge construtor
		public int edges;

		// summed in Edge constructor
		public Vector3 edge_center_sum;

		// calculated at subdivision
		public Vert descendant;

		// used during Mesh conversion
		public int order;

		public Vert ( Vector3 position, int n = 4 )
		{
			this.position = position;

			adjacencies = new List<Adjacency>( n );

			//Debug.Log( "new Vertex " + position );
		}

		public Vert ( Vector3 position, Vector2 uv, Color color, int n = 4 ) : this( position, n )
		{
			this.uv = uv;
			this.color = color;
		}
		
		public bool TryGetAdjacency( Vert vertex, out Adjacency result )
		{
			foreach ( var adjacency in adjacencies )
			{
				if ( adjacency.vertex == vertex )
				{
					result = adjacency;
					return true;
				}
			}

			result = default( Adjacency );
			return false;
		}
		
		public bool TryGetAdjacency( Edge edge, out Adjacency result )
		{
			foreach ( var adjacency in adjacencies )
			{
				if ( adjacency.edge == edge )
				{
					result = adjacency;
					return true;
				}
			}

			result = default( Adjacency );
			return false;
		}
		
		public bool TryGetAdjacency( Face face, out Adjacency result )
		{
			foreach ( var adjacency in adjacencies )
			{
				if ( adjacency.face == face )
				{
					result = adjacency;
					return true;
				}
			}

			result = default( Adjacency );
			return false;
		}

		public void DrawGizmo ()
		{
			Gizmos.DrawCube( position, Vector3.one * 0.01f );

			Gizmos.color = Color.cyan;

			for ( int i = 0; i < adjacencies.Count; i++ )
			{
				Gizmos.DrawLine( position, adjacencies[i].face.point.position );
			}

			Gizmos.color = Color.white;
		}
	}
}
