﻿using UnityEngine;

namespace ToBoldlyPlay.Procedural.CatmullClark
{
	public class Face
	{
		public Vert[] vertices;

		public Edge[] edges;

		public Vert point;

		public Face () { }

		public Face ( Surface surface, params Vert[] vertices )
		{
			this.vertices = vertices;

			edges = new Edge[vertices.Length];

			point = new Vert( Vector3.zero );

			for ( int i = 0; i < vertices.Length; i++ )
			{
				Adjacency adjacency;

				Vert vertex = vertices[i == vertices.Length - 1 ? 0 : i + 1];

				Edge edge;

				if ( vertex.TryGetAdjacency( vertices[i], out adjacency ) )
				{
					edge = adjacency.edge;
				}
				else
				{
					surface.edges.Add( edge = new Edge( vertices[i], vertex ) );
				}

				edge.faces.Add( this );
				this.vertices[i].adjacencies.Add( new Adjacency( vertex, edge, this ) );
				this.edges[i] = edge;

				point.position += vertices[i].position;
				point.uv += vertices[i].uv;
				//point.tangent += vertices[i].tangent;
				point.color += vertices[i].color;

				//Debug.Log( "new Adjacency " + vertices[i].position + ">" + vertex.position );
			}

			point.position /= vertices.Length;
			point.uv /= vertices.Length;
			//point.tangent /= vertices.Length;
			point.color /= vertices.Length;

			surface.faces.Add( this );
		}
	}
}
