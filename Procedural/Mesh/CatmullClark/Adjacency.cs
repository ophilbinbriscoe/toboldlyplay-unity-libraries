﻿namespace ToBoldlyPlay.Procedural.CatmullClark
{
	public struct Adjacency
	{
		public Vert vertex;

		public Edge edge;

		public Face face;

		public Adjacency ( Vert vertex, Edge edge, Face face )
		{
			this.vertex = vertex;
			this.edge = edge;
			this.face = face;
		}
	}
}
