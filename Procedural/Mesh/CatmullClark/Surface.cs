﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToBoldlyPlay.Procedural.CatmullClark
{
	public partial class Surface
	{
		public List<Vert> verts;
		public List<Edge> edges;
		public List<Face> faces;

		public Surface ( int verts, int edges, int faces )
		{
			this.verts = new List<Vert>( verts );
			this.edges = new List<Edge>( edges );
			this.faces = new List<Face>( faces );
		}

		public Surface ( Surface surface )
		{
			// strictly
			verts = new List<Vert>( surface.verts.Count + surface.edges.Count + surface.faces.Count  );

			// 2e + 3f <= actual <= 2e + 4f
			edges = new List<Edge>( surface.edges.Count * 2 + surface.faces.Count * 4 );

			// 3f <= actual <= 4f
			faces = new List<Face>( surface.faces.Count * 4 );
		}

		public void DrawVertexGizmos ()
		{
			foreach ( var vertex in verts )
			{
				vertex.DrawGizmo();
			}
		}

		public void DrawEdgeGizmos ()
		{
			foreach ( var edge in edges )
			{
				edge.DrawGizmo();
			}
		}
	}
}
