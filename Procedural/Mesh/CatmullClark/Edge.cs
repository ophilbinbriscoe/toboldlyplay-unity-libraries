﻿using System.Collections.Generic;
using UnityEngine;

namespace ToBoldlyPlay.Procedural.CatmullClark
{
	public class Edge
	{
		// calculated at construction
		public Vector3 center;

		// undirected
		public Vert a, b;

		// calculated during subdivision
		public Vert point;

		public List<Face> faces;

		public Edge ( Vert a, Vert b )
		{
			this.a = a;
			this.b = b;

			center = (a.position + b.position) / 2.0f;

			faces = new List<Face>( 2 );

			a.edges++;
			b.edges++;

			a.edge_center_sum += center;
			b.edge_center_sum += center;

			//Debug.Log( "new Edge " + a.position + "_" + b.position );
		}

		public void DrawGizmo()
		{
			Gizmos.color = faces.Count == 2 ? Color.white : Color.red;

			Gizmos.DrawLine( a.position, b.position );

			Gizmos.color = Color.magenta;

			for ( int i = 0; i < faces.Count; i++ )
			{
				Gizmos.DrawLine( ( a.position + b.position ) / 2, faces[i].point.position );
			}

			Gizmos.color = Color.white;
		}
	}
}
