﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace ToBoldlyPlay.Procedural
{
	public static partial class MeshExtensions
	{
		/// <summary>
		/// Apply a translation transformation to a Mesh.
		/// </summary>
		/// <param name="mesh">Mesh to transform.</param>
		/// <param name="translation">Translation.</param>
		public static void Translate( this Mesh mesh, Vector3 translation )
		{
			Transform ( mesh, Matrix4x4.TRS( translation, Quaternion.identity, Vector3.one ) );
		}
	
		/// <summary>
		/// Apply a translation transformation to a Mesh.
		/// </summary>
		/// <param name="mesh">Mesh to transform.</param>
		/// <param name="rotation">Rotation.</param>
		public static void Rotate( this Mesh mesh, Quaternion rotation )
		{
			Transform ( mesh, Matrix4x4.TRS( Vector3.zero, rotation, Vector3.one ) );
		}
		
		/// <summary>
		/// Apply a translation transformation to a Mesh.
		/// </summary>
		/// <param name="mesh">Mesh to transform.</param>
		/// <param name="scale">Scale.</param>
		public static void Scale( this Mesh mesh, Vector3 scale )
		{
			Transform ( mesh, Matrix4x4.TRS( Vector3.zero, Quaternion.identity, scale ) );
		}
	
		/// <summary>
		/// Localize a Mesh
		/// </summary>
		/// <param name="mesh">Mesh to transform.</param>
		/// <param name="transform">Target local space.</param>
		public static void Localize( this Mesh mesh, Transform transform )
		{
			Transform( mesh, transform.localToWorldMatrix );
		}
	
		/// <summary>
		/// Delocalize a Mesh.
		/// </summary>
		/// <param name="mesh">Mesh to transform.</param>
		/// <param name="transform">Current local space.</param>
		public static void Delocalize( this Mesh mesh, Transform transform )
		{
			Transform( mesh, transform.worldToLocalMatrix );
		}
	
		/// <summary>
		/// Apply a transformation to a Mesh.
		/// </summary>
		/// <param name="mesh">Mesh to transform.</param>
		/// <param name="matrix">Transformation matrix</param>
		public static void Transform( this Mesh mesh, Matrix4x4 matrix )
		{
			var vertices = new Vector3[ mesh.vertices.Length ];
			var normals = new Vector3[ mesh.normals.Length ];
			var tangents = new Vector4[ mesh.tangents.Length ];
		
			if ( matrix != Matrix4x4.identity )
			{
				for( int i = 0; i < vertices.Length; i++ )
				{
					vertices[i] = matrix.MultiplyPoint3x4( mesh.vertices[i] );
					normals[i] = matrix.MultiplyPoint3x4( mesh.normals[i] );
					tangents[i] = matrix.MultiplyPoint3x4( mesh.tangents[i] );
				}
			}
		
			mesh.vertices = vertices;
			mesh.normals = normals;
			mesh.tangents = tangents;
		}
	
		/// <summary>
		/// Invert all of a Mesh's normals.
		/// </summary>
		/// <param name="mesh">Mesh to perform inversion on.</param>
		public static void InvertNormals( Mesh mesh )
		{		
			Vector3[] normals = mesh.normals.Clone() as Vector3[];
			int[] triangles = mesh.triangles.Clone() as int[];
		
			for( int i = 0; i < mesh.normals.Length; i++ )
			{
				normals[i] = -mesh.normals[i];
			}
		
			for( int i = 0; i < mesh.triangles.Length; i += 3 )
			{
				triangles[i] = mesh.triangles[i+2];
				triangles[i+2] = mesh.triangles[i];
			}
		
			mesh.normals = normals;
			mesh.triangles = triangles;
		}
	
		/// <summary>
		/// Copy all of a Mesh's data to another Mesh
		/// </summary>
		/// <param name="from">Source Mesh.</param>
		/// <param name="to">Target Mesh.</param>
		public static void CopyTo( this Mesh from, Mesh to )
		{
			to.Clear();
		
			to.vertices = from.vertices.Clone() as Vector3[];
			to.normals = from.normals.Clone() as Vector3[];
			to.colors = from.colors.Clone() as Color[];
			to.uv = from.uv.Clone() as Vector2[];
			to.uv2 = from.uv2.Clone() as Vector2[];
			to.uv3 = from.uv3.Clone() as Vector2[];
			to.uv4 = from.uv4.Clone() as Vector2[];
			to.tangents = from.tangents.Clone() as Vector4[];
		
			to.triangles = (int[]) from.triangles.Clone();
		}
	
		/// <summary>
		/// Calculate tangent vectors for a Mesh based on vertex position, normal, and uv data.
		/// 
		/// Credit: http://answers.unity3d.com/questions/7789/calculating-tangents-vector4.html
		/// </summary>
		/// <param name="mesh">Mesh to calculate tangents for.</param>
		public static void RecalculateTangents( this Mesh mesh )
		{
			//speed up math by copying the mesh arrays
			int[] triangles = mesh.triangles;
			Vector3[] vertices = mesh.vertices;
			Vector2[] uv = mesh.uv;
			Vector3[] normals = mesh.normals;
		
			//variable definitions
			int triangleCount = triangles.Length;
			int vertexCount = vertices.Length;
		
			Vector3[] tan1 = new Vector3[vertexCount];
			Vector3[] tan2 = new Vector3[vertexCount];
		
			Vector4[] tangents = new Vector4[vertexCount];
		
			for (long a = 0; a < triangleCount; a += 3)
			{
				long i1 = triangles[a + 0];
				long i2 = triangles[a + 1];
				long i3 = triangles[a + 2];
			
				Vector3 v1 = vertices[i1];
				Vector3 v2 = vertices[i2];
				Vector3 v3 = vertices[i3];
			
				Vector2 w1 = uv[i1];
				Vector2 w2 = uv[i2];
				Vector2 w3 = uv[i3];
			
				float x1 = v2.x - v1.x;
				float x2 = v3.x - v1.x;
				float y1 = v2.y - v1.y;
				float y2 = v3.y - v1.y;
				float z1 = v2.z - v1.z;
				float z2 = v3.z - v1.z;
			
				float s1 = w2.x - w1.x;
				float s2 = w3.x - w1.x;
				float t1 = w2.y - w1.y;
				float t2 = w3.y - w1.y;
			
				float r = 1.0f / (s1 * t2 - s2 * t1);
			
				Vector3 sdir = new Vector3((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
				Vector3 tdir = new Vector3((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);
			
				tan1[i1] += sdir;
				tan1[i2] += sdir;
				tan1[i3] += sdir;
			
				tan2[i1] += tdir;
				tan2[i2] += tdir;
				tan2[i3] += tdir;
			}
		
		
			for (long a = 0; a < vertexCount; ++a)
			{
				Vector3 n = normals[a];
				Vector3 t = tan1[a];
			
				//Vector3 tmp = (t - n * Vector3.Dot(n, t)).normalized;
				//tangents[a] = new Vector4(tmp.x, tmp.y, tmp.z);
				Vector3.OrthoNormalize(ref n, ref t);
				tangents[a].x = t.x;
				tangents[a].y = t.y;
				tangents[a].z = t.z;
			
				tangents[a].w = (Vector3.Dot(Vector3.Cross(n, t), tan2[a]) < 0.0f) ? -1.0f : 1.0f;
			}
		
			mesh.tangents = tangents;
		}
	}
}
