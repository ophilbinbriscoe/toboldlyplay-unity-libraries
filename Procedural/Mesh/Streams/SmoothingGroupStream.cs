﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ToBoldlyPlay.Procedural
{
	using SmoothingGroup = Dictionary<Vector3, int>;

	/// <summary>
	/// Easy (but expensive) class for creating distinct smooth surfaces.
	/// </summary>
	public class SmoothingGroupStream : MeshStream
	{
		protected List<SmoothingGroup> groups;
		protected SmoothingGroup activeGroup;

		public SmoothingGroupStream ()
		{
			groups = new List<SmoothingGroup>();

			activeGroup = new SmoothingGroup();

			groups.Add( activeGroup );
		}

		/// <summary>
		/// Begin a new smoothing group to be checked against during vertex creation operations.
		/// </summary>
		/// <returns>The new group index.</returns>
		public int BeginGroup ()
		{
			activeGroup = new SmoothingGroup();

			groups.Add( activeGroup );

			return groups.Count - 1;
		}

		/// <summary>
		/// Set the smoothing group to be checked against during subequent vertex creation operations.
		/// </summary>
		/// <param name="group">The group index.</param>
		public void SetGroup ( int group )
		{
			activeGroup = groups[Mathf.Clamp( group, 0, groups.Count - 1 )];
		}

		/// <summary>
		/// Guarantees only one vertex can occupy a given point in space within the active group.
		/// </summary>
		/// <param name="position"></param>
		/// <returns></returns>
		public override IMeshStream Vertex ( Vector3 position )
		{
			if ( activeGroup.TryGetValue( position, out index ) )
			{
				return this;
			}

			positionData.Add( position );

			activeGroup[position] = index = positionData.Count - 1;

			return this;
		}
	}
}
