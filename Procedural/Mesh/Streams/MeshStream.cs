﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace ToBoldlyPlay.Procedural
{
	public enum VertexDataFlags : byte
	{
		NONE	= 0,
		UV		= 1,
		NORMAL	= 2,
		TANGENT	= 4,
		COLOR	= 8
	};

	public interface IMeshStream
	{
		int VertexCount { get; }
		int TriangleCount { get; }

		IMeshStream Vertex ( Vector3 position );

		IMeshStream AddUV ( Vector2 uv );
		IMeshStream AddNormal ( Vector3 normal );
		IMeshStream AddTangent ( Vector4 tangent );
		IMeshStream AddColor ( Color color );

		int Index ();

		void Triangle ( int a, int b, int c );

		void CopyTo ( Mesh mesh, bool calculateNormals, bool calculateTangents, bool calculateBounds, bool clear );
	}

	/// <summary>
	/// Provides a fluid interface for defining vertex data.
	/// </summary>
	public class MeshStream : IMeshStream
	{
		protected List<Vector3> positionData;
		protected List<int> triangleData;
		protected List<Vector2> uvData;
		protected List<Vector3> normalData;
		protected List<Vector4> tangentData;
		protected List<Color> colorData;

		protected int index;

		public int VertexCount
		{
			get
			{
				return positionData.Count;
			}
		}

		public int TriangleCount
		{
			get
			{
				return triangleData.Count / 3;
			}
		}

		public MeshStream ( bool uvs = false, bool normals = false, bool tangents = false, bool colors = false )
		{
			positionData = new List<Vector3>();
			triangleData = new List<int>();

			if ( uvs )
			{
				uvData = new List<Vector2>();
			}

			if ( normals )
			{
				normalData = new List<Vector3>();
			}

			if ( tangents )
			{
				tangentData = new List<Vector4>();
			}

			if ( colors )
			{
				colorData = new List<Color>();
			}
		}
		
		/// <summary>
		/// Copy this MeshStream's data into a Mesh.
		/// </summary>
		/// <param name="mesh">The Mesh to copy to.</param>
		/// <param name="calculateNormals">Whether or not to calculate normals from triangle data.</param>
		/// <param name="calculateTangents">Whether or not to calculate tangents from normal and uv data.</param>
		/// <param name="calculateBounds">Whether or not to calculate the Mesh's bounds. (recommended)</param>
		/// <param name="clear">Whether or not the Mesh should be cleared. Should only set to false if the new vertex count is identical to the existing vertex count.</param>
		public virtual void CopyTo( Mesh mesh, bool calculateNormals = true, bool calculateTangents = true, bool calculateBounds = true, bool clear = true )
		{
			if ( clear )
			{
				mesh.Clear();
			}

			mesh.SetVertices( positionData );
			mesh.SetTriangles( triangleData, 0 );

			if ( uvData != null )
			{
				mesh.SetUVs( 1, uvData );
			}
			else
			{
				mesh.uv = new Vector2[positionData.Count];
			}

			if ( normalData != null )
			{
				mesh.SetNormals( normalData );
			}
			else
			{
				mesh.normals = new Vector3[positionData.Count];
			}

			if ( tangentData != null )
			{
				mesh.SetTangents( tangentData );
			}
			else
			{
				mesh.tangents = new Vector4[positionData.Count];
			}

			if ( colorData != null )
			{
				mesh.SetColors( colorData );
			}
			else
			{
				var colors32 = new Color32[positionData.Count];

				for ( int i = 0; i < positionData.Count; i++ )
				{
					colors32[i] = new Color32( 255, 255, 255, 255 );
				}

				mesh.colors32 = colors32;
			}

			if ( calculateBounds )
			{
				mesh.RecalculateBounds();
			}

			if ( calculateNormals )
			{
				mesh.RecalculateNormals();
			}

			if ( calculateTangents )
			{
				mesh.RecalculateTangents();
			}
		}

		public virtual void ModifyPosition ( int i, Vector3 position )
		{
			positionData[i] = position;
		}

		public virtual void ModifyUV ( int i, Vector2 uv )
		{
			uvData[i] = uv;
		}

		public virtual void ModifyNormal ( int i, Vector3 normal )
		{
			normalData[i] = normal;
		}

		public virtual void ModifyTangent ( int i, Vector4 tangent )
		{
			tangentData[i] = tangent;
		}

		public virtual void ModifyColor ( int i, Color color )
		{
			colorData[i] = color;
		}
		
		/// <summary>
		/// Define a new vertex using its position.
		/// </summary>
		/// <param name="position">Vertex position.</param>
		/// <returns>The interface called upon.</returns>
		public virtual IMeshStream Vertex( Vector3 position )
		{
			positionData.Add( position );

			index = positionData.Count - 1;

			return this;
		}
		
		/// <summary>
		/// Specify color data for the most recently defined vertex.
		/// </summary>
		/// <param name="uv">Vertex UV coordinate.</param>
		/// <returns>The interface called upon.</returns>
		public virtual IMeshStream AddUV ( Vector2 uv )
		{
			uvData.Add( uv );

			return this;
		}
		
		/// <summary>
		/// Specify color data for the most recently defined vertex.
		/// </summary>
		/// <param name="normal">Vertex normal vector.</param>
		/// <returns>The interface called upon.</returns>
		public virtual IMeshStream AddNormal ( Vector3 normal )
		{
			normalData.Add( normal );

			return this;
		}
		
		/// <summary>
		/// Specify color data for the most recently defined vertex.
		/// </summary>
		/// <param name="tangent">Vertex tangent vector.</param>
		/// <returns>The interface called upon.</returns>
		public virtual IMeshStream AddTangent ( Vector4 tangent )
		{
			tangentData.Add( tangent );

			return this;
		}

		/// <summary>
		/// Specify color data for the most recently defined vertex.
		/// </summary>
		/// <param name="color">Vertex color.</param>
		/// <returns>The interface called upon.</returns>
		public virtual IMeshStream AddColor ( Color color )
		{
			colorData.Add( color );

			return this;
		}

		/// <summary>
		/// Query the index of the most recently defined vertex.
		/// </summary>
		/// <returns>Most recently created vertex's index.</returns>
		public virtual int Index ()
		{
			return index;
		}

		/// <summary>
		/// Create a Triangle using 3 vertex indices.
		/// </summary>
		/// <param name="a">1st index.</param>
		/// <param name="b">2nd index.</param>
		/// <param name="c">3rd index.</param>
		public virtual void Triangle( int a, int b, int c )
		{
			triangleData.Add( a );
			triangleData.Add( b );
			triangleData.Add( c );
		}
	}
}
