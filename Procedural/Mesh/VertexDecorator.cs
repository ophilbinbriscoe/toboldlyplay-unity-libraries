﻿using UnityEngine;

namespace ToBoldlyPlay.Procedural
{
	public interface IVertexDecorator
	{
		IMeshStream Decorate ( IMeshStream stream );
	}
}
