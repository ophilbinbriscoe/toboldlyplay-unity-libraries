﻿using UnityEngine;
using UnityEditor;
using System.IO;

namespace ToBoldlyPlay.EditorExtensions
{
    public static partial class Create
	{
		/// <summary>
		/// Create an asset file under the currently highighted project folder, with a ScriptableObject derived type as its root.
		/// </summary>
		/// <typeparam name="T">Fully derived type of asset's root Object.</typeparam>
		/// <param name="focus">Whether or not to select the asset in the editor.</param>
		/// <returns>The asset's root Object.</returns>
		public static T CreateAsset<T>( bool focus = false ) where T : ScriptableObject
		{
			string path = AssetDatabase.GetAssetPath (Selection.activeObject);

			if ( path == "" ) 
			{
				path = "Assets";
			} 
			else if ( Path.GetExtension(path) != "" ) 
			{
				path = path.Replace( Path.GetFileName( AssetDatabase.GetAssetPath( Selection.activeObject ) ), "" );
			}

			return CreateAsset<T>( path + "/New " + typeof( T ).Name + ".asset", focus );
		}

		/// <summary>
		/// Create an asset file with a ScriptableObject derived type as its root.
		/// </summary>
		/// <typeparam name="T">Fully derived type of asset's root Object.</typeparam>
		/// <param name="path">The path (including file name & extension) of the new asset file.</param>
		/// <param name="focus">Whether or not to select the asset in the editor.</param>
		/// <returns>The asset's root Object.</returns>
		/// <returns></returns>
		public static T CreateAsset<T>( string path, bool focus = false ) where T : ScriptableObject
		{
			var asset = ScriptableObject.CreateInstance<T>();

			AssetDatabase.CreateAsset( asset, path );
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();

			if ( focus )
			{
				EditorUtility.FocusProjectWindow();
				Selection.activeObject = asset;
			}

			return asset;
		}
	}
}
