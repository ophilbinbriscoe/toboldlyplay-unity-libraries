﻿using UnityEngine;
using System;

namespace ToBoldlyPlay.EditorExtensions
{
	[AttributeUsage( AttributeTargets.Field, AllowMultiple = false, Inherited = true )]
	public class ListAttribute : PropertyAttribute
	{
		public readonly int min;
		public readonly int max;

		public ListAttribute ( int min = 0, int max = int.MaxValue )
		{
			this.min = min;
			this.max = max;
		}
	}
}
