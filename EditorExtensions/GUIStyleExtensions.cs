﻿using UnityEngine;

namespace ToBoldlyPlay.EditorExtensions
{
	public static class GUIStyleExtensions
	{
		public static float CalcHeight ( this GUIStyle style, string content = "Lorem_Ipsum^123", float width = 100.0f )
		{
			return style.CalcHeight( new GUIContent( content ), width );
		}
	}
}
