﻿using System;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Reflection;

namespace ToBoldlyPlay.EditorExtensions
{
	/// <summary>
	/// Allows built-in GUIStyles to be previewed.
	/// </summary>
	[Serializable]
	public class GUIStyleBrowserWindow : EditorWindow, ISerializationCallbackReceiver
	{
		[MenuItem ("Window/GUI Style Browser")]
		static void Open ()
		{
			GUIStyleBrowserWindow window = GetWindow<GUIStyleBrowserWindow>();

			//setup window

			window.name = "GUI Style Browser";
			window.titleContent = new GUIContent( "GUI Style Browser" );
			
			window.Show();
		}

		private Dictionary<string,GUIStyle> styles;

		[SerializeField] private Vector2 scrollPosition;
		[SerializeField] private string input;
		[SerializeField] private string display;

		void OnEnable ()
		{
			styles = new Dictionary<string, GUIStyle>();

			foreach ( var field in typeof( EditorStyles ).GetFields( BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static ) )
			{
				if ( typeof( GUIStyle ).IsAssignableFrom( field.FieldType ) )
				{
					var style = field.GetValue( null ) as GUIStyle;

					if ( style != null )
					{
						styles["(EditorStyles) " + style.name] = style;
					}
				}
			}

			foreach ( var value in Enum.GetValues( typeof( EditorSkin ) ) as EditorSkin[] )
			{
				GUISkin skin = EditorGUIUtility.GetBuiltinSkin( value );

				foreach ( var style in skin.customStyles )
				{
					if ( style != null )
					{
						styles[ "(" + skin.name + ") " + style.name] = style;
					}
				}
			}

			if ( input == null )
			{
				input = "";
			}

			if ( display == null )
			{
				display = "Lorem Ipsum 123";
			}
		}

		void OnGUI ()
		{
			var toolbar = EditorGUILayout.BeginHorizontal();

			GUI.SetNextControlName( "search_text_field" );

			GUI.Box( new Rect( 0, 0, position.width, toolbar.height ), GUIContent.none, EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).GetStyle( "Toolbar" ) );

			string focus = GUI.GetNameOfFocusedControl();

			input = EditorGUILayout.TextField( "Search", input, EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).GetStyle( "ToolbarSeachTextField" ) );

			GUI.SetNextControlName( "search_cancel_button" );

			if ( GUILayout.Button( GUIContent.none, string.IsNullOrEmpty( input ) ?  EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).GetStyle( "ToolbarSeachCancelButtonEmpty" ) :  EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).GetStyle( "ToolbarSeachCancelButton" ) ) )
			{
				input = "";
				
				if ( focus == "search_text_field" )
				{
					GUI.FocusControl( "search_cancel_button" );
				}
			}

			EditorGUILayout.EndHorizontal();

			EditorGUILayout.Separator();

			scrollPosition = EditorGUILayout.BeginScrollView( scrollPosition );

			foreach ( var pair in styles )
			{
				if ( !string.IsNullOrEmpty( input ) && !pair.Key.ToLower().Contains( input.ToLower() ) )
				{
					continue;
				}

				EditorGUILayout.BeginHorizontal();

				EditorGUILayout.SelectableLabel( pair.Key, GUILayout.Width( 256f ) );

				if ( GUILayout.Button( display, pair.Value, GUILayout.Height( 32f ) ) )
				{
					EditorGUIUtility.systemCopyBuffer = pair.Value.name;
				}

				EditorGUILayout.EndHorizontal();

				GUILayout.Space( 10f );
			}

			EditorGUILayout.EndScrollView();
		}

		void ISerializationCallbackReceiver.OnAfterDeserialize () { }

		void ISerializationCallbackReceiver.OnBeforeSerialize () { }
	}
}
