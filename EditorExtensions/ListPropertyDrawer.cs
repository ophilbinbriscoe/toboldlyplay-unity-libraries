﻿using System;
using UnityEditor;
using UnityEngine;

namespace ToBoldlyPlay.EditorExtensions
{
	[CustomPropertyDrawer( typeof( ListAttribute ), true )]
	public class ListPropertyDrawer : PropertyDrawer
	{
		public override void OnGUI ( Rect position, SerializedProperty property, GUIContent label )
		{
			if ( !property.isArray )
			{
				base.OnGUI( position, property, label );
				return;
			}

			property.isExpanded = EditorGUI.Foldout( new Rect( position.x, position.y, position.width, EditorGUIUtility.singleLineHeight ), property.isExpanded, label );

			if ( property.isExpanded )
			{
				throw new NotImplementedException();
			}
		}

		public override float GetPropertyHeight ( SerializedProperty property, GUIContent label )
		{
			return base.GetPropertyHeight( property, label ) + (property.isExpanded ? GUI.skin.button.CalcHeight() : 0.0f );
		}
	}
}
