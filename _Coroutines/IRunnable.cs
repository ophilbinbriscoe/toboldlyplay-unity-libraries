﻿using System;

namespace ToBoldlyPlay.Coroutines
{
	public interface IRunnable
	{
		event Action<IRunnable> OnDone;

		bool IsDone { get; }
		bool IsRunning { get; }

		void Run ();
		void Halt ();
		void Reset ();
	}
}