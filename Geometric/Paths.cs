﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace ToBoldlyPlay.Geometric
{
	public static partial class Geometry
	{
		/// <summary>
		/// Test if a path defines a closed polygon.
		/// </summary>
		/// <param name="path">Path to test.</param>
		/// <returns>True, if firt and last points are equal.</returns>
		public static bool IsClosed ( this Vector3[] path )
		{
			return path.Last() == path.First();
		}

		/// <summary>
		/// Close a path.
		/// </summary>
		/// <param name="path">Path to close.</param>
		/// <returns>The path, with the last point equal to the first.</returns>
		public static Vector3[] Close ( this Vector3[] path )
		{
			if ( ! path.IsClosed() )
			{
				var array = new Vector3[path.Length + 1];
				path.CopyTo( array, 0 );
				array[path.Length] = path[0];
				return array;
			}

			return path;
		}

		/// <summary>
		/// Sum of the lengths of the line segments defined by a path.
		/// </summary>
		/// <param name="path">Path to sum.</param>
		/// <returns>Total length.</returns>
		public static float Length ( this Vector3[] path )
		{
			float length = 0f;

			for ( int i = 1; i < path.Count(); i++ )
			{
				length += Vector3.Distance( path[i], path[i - 1] );
			}

			return length;
		}

		/// <summary>
		/// Average of all points in a path.
		/// </summary>
		/// <param name="path">Path to average.</param>
		/// <returns>Average as point.</returns>
		public static Vector3 Average ( this Vector3[] path )
		{
			float x = 0f, y = 0f, z = 0f;

			foreach( Vector3 p in path )
			{
				x += p.x;
				y += p.y;
				z += p.z;
			}

			return new Vector3( x, y, z ) / path.Length;
		}

		/// <summary>
		/// The point at a certain displacement along a path.
		/// </summary>
		/// <param name="path">Path.</param>
		/// <param name="displacement">Displacement.</param>
		/// <returns>Displaced point.</returns>
		public static Vector3 PointAt( Vector3[] path, float displacement )
		{
			if ( displacement <= 0f ) return path.First();
			
			float L = path.Length();
		
			float d = 0f;
			
			for ( int a = 0, b; a < path.Length - 1; a++ )
			{
				b = a + 1;
				
				if ( path[a] == path[b] ) continue;

				float m = Vector3.Distance( path[a], path[b] );
				
				if ( displacement <= d + m )
				{
					return Vector3.Lerp( path[a], path[b], (displacement - d) / m );
				}
				
				d += m;
			}
			
			return path.Last();
		}

		/// <summary>
		/// Test if a path contains a point. (Not implemented)
		/// </summary>
		/// <param name="path">Path.</param>
		/// <param name="point">Point.</param>
		/// <returns>True, if the path contains the point.</returns>
		public static bool Contains( Vector3[] path, Vector3 point )
		{
			//int n = path.IsClosed() ? path.Length - 1 : path.Length;

			//foreach( Triangle triangle in Triangulation.Triangles( path, Triangulation.EarClip ) )
			//{
			//	if ( triangle.Contains( point ) ) return true;
			//}

			throw new NotImplementedException();
		}
		
		public static Vector3[] Invert( this Vector3[] path )
		{	
			Vector3[] p2  = new Vector3[path.Length];

			for ( int i = 0; i < p2.Length; )
			{
				p2[i] = path[p2.Length - (++i)];
			}

			return p2;
		}
		
		public static Vector3[] Displace( this Vector3[] path, Vector3 vector )
		{
			Vector3[] p2 = path.Clone() as Vector3[];

			for ( int i = 0; i < p2.Length; i++ )
			{
				p2[i] += vector;
			}

			return p2;
		}
		
		public static Vector3[] Scale( this Vector3[] path, float scale )
		{
			Vector3[] p2 = path.Clone() as Vector3[];

			for ( int i = 0; i < p2.Length; i++ )
			{
				p2[i] *= scale;
			}

			return p2;
		}
		
		public static Vector3[] Scale( this Vector3[] path, Vector3 scale )
		{
			Vector3[] p2 = path.Clone() as Vector3[];

			for ( int i = 0; i < p2.Length; i++ )
			{
				p2[i] = new Vector3( p2[i].x * scale.x, p2[i].y * scale.y, p2[i].z * scale.z );
			}

			return p2;
		}
		
		public static Vector3[] Scale ( this Vector3[] path, float scale, Vector3 zero )
		{
			Vector3[] p2 = path.Clone() as Vector3[];

			for ( int i = 0; i < path.Length; i++ )
			{
				p2[i] = zero + (p2[i] - zero) * scale;
			}

			return p2;
		}

		
		public static Vector3[] Scale( this Vector3[] path, Vector3 scale, Vector3 zero )
		{
			Vector3[] p2 = path.Clone() as Vector3[];

			for ( int i = 0; i < p2.Length; i++ )
			{
				p2[i] = zero + (p2[i] - zero);
				p2[i] = new Vector3( p2[i].x * scale.x, p2[i].y * scale.y, p2[i].z * scale.z );
			}

			return p2;
		}

		public static Vector3[] Rotate( this Vector3[] path, Quaternion quaternion )
		{
			Vector3[] p2 = path.Clone() as Vector3[];

			for ( int i = 0; i < p2.Length; i++ )
			{
				p2[i] = quaternion * path[i];
			}

			return p2;
		}
		
		public static Vector3[] Transform( this Vector3[] path, Matrix4x4 matrix )
		{
			Vector3[] p2 = path.Clone() as Vector3[];

			for ( int i = 0; i < p2.Length; i++ )
			{
				p2[i] = matrix.MultiplyPoint3x4( path[i] );
			}

			return p2;
		}

		/// <summary>
		/// Normal of implicit surface defined by planar path.
		/// </summary>
		/// <param name="path">Path to take the normal of.</param>
		/// <returns>Normal as vector.</returns>
		public static Vector3 Normal ( this Vector3[] path )
		{
			if ( path.Length == 2 )
			{
				return Vector3.zero;
			}

			Vector3 n = Vector3.zero;

			for ( int i = 2; i < path.Count(); i++ )
			{
				if ( (n = Normal( path[i - 2], path[i - 1], path[i] )) != Vector3.zero )
				{
					break;
				}
			}

			path.Close();

			float area = SignedArea( path, n );
			
			return area > 0f ? n : (area == 0f ? Vector3.zero : - n );
		}

		public static Vector2[] Flatten ( this Vector3[] path )
		{
			if ( path.Length > 2 )
			{
				return Flatten( path, new Triangle( path[0], path[1], path[2] ).Normal() );
			}
			else if ( path.Length > 1 )
			{
				Vector3 delta = path[1] - path[0];

				if ( delta.x < delta.y && delta.x < delta.z )
				{
					return new Vector2[] { new Vector2( path[0].y, path[0].z ), new Vector2( path[1].y, path[1].z ) };
				}
				else if ( delta.y < delta.z )
				{
					return new Vector2[] { new Vector2( path[0].x, path[0].z ), new Vector2( path[1].x, path[1].z ) };
				}
				else
				{
					return new Vector2[] { new Vector2( path[0].x, path[0].y ), new Vector2( path[1].x, path[1].y ) };
				}
			}
			else
			{
				if ( path[0].x < path[0].y && path[0].x < path[0].z )
				{
					return new Vector2[] { new Vector2( path[0].y, path[0].z ) };
				}
				else if ( path[0].y < path[0].z )
				{
					return new Vector2[] { new Vector2( path[0].x, path[0].z ) };
				}
				else
				{
					return new Vector2[] { new Vector2( path[0].x, path[0].y ) };
				}
			}
		}

		public static Vector2[] Flatten ( this Vector3[] path, Vector3 normal )
		{
			Vector2[] flat = new Vector2[path.Length];

			if ( normal.x > normal.y && normal.x > normal.z )
			{
				for ( int i = 0; i < path.Length; i++ )
				{
					flat[i] = new Vector2( path[i].y, path[i].z );
				}
			}
			else if ( normal.y > normal.z )
			{
				for ( int i = 0; i < path.Length; i++ )
				{
					flat[i] = new Vector2( path[i].x, path[i].z );
				}
			}
			else
			{
				for ( int i = 0; i < path.Length; i++ )
				{
					flat[i] = new Vector2( path[i].x, path[i].y );
				}
			}

			return flat;
		}

		/// <summary>
		/// Signed area of the polygon defined by a path.
		/// 
		/// Credit: http://geomalgorithms.com/a01-_area.html
		/// </summary>
		/// <param name="path">Path to take the signed area of.</param>
		/// <param name="N">Normal vector.</param>
		/// <returns>Signed area.</returns>
		public static float SignedArea( this Vector3[] path, Vector3 N )
		{
			int n = path.Length - 1;
			float area = 0;
			float an, ax, ay, az; // abs value of normal and its coords
			int  coord;           // coord to ignore: 1=x, 2=y, 3=z
			int  i, j, k;         // loop indices
		
			if (n < 3) return 0;  // a degenerate polygon
		
			// select largest abs coordinate to ignore for projection
			ax = (N.x>0 ? N.x : -N.x);    // abs x-coord
			ay = (N.y>0 ? N.y : -N.y);    // abs y-coord
			az = (N.z>0 ? N.z : -N.z);    // abs z-coord
		
			coord = 3;                    // ignore z-coord
			if (ax > ay) {
				if (ax > az) coord = 1;   // ignore x-coord
			}
			else if (ay > az) coord = 2;  // ignore y-coord
		
			// compute area of the 2D projection
			switch (coord) {
			case 1:
				for (i=1, j=2, k=0; i<n; i++, j++, k++)
					area += (path[i].y * (path[j].z - path[k].z));
				break;
			case 2:
				for (i=1, j=2, k=0; i<n; i++, j++, k++)
					area += (path[i].z * (path[j].x - path[k].x));
				break;
			case 3:
				for (i=1, j=2, k=0; i<n; i++, j++, k++)
					area += (path[i].x * (path[j].y - path[k].y));
				break;
			}
			switch (coord) {    // wrap-around term
			case 1:
				area += (path[n].y * (path[1].z - path[n-1].z));
				break;
			case 2:
				area += (path[n].z * (path[1].x - path[n-1].x));
				break;
			case 3:
				area += (path[n].x * (path[1].y - path[n-1].y));
				break;
			}
		
			// scale to get area before projection
			an = Mathf.Sqrt( ax*ax + ay*ay + az*az ); // length of normal vector
			switch (coord) {
			case 1:
				area *= (an / (2 * N.x));
				break;
			case 2:
				area *= (an / (2 * N.y));
				break;
			case 3:
				area *= (an / (2 * N.z));
				break;
			}
			return area;
		}

		/// <summary>
		/// Create a path from a list of elements.
		/// </summary>
		/// <typeparam name="T">Element type.</typeparam>
		/// <param name="list">List.</param>
		/// <param name="function">Function.</param>
		/// <param name="close">Whether or not the last point should be a duplicate of the first point.</param>
		/// <returns>The new path.</returns>
		public static Vector3[] ToPath<T>( this IList<T> list, Func<T,Vector3> function, bool close = false )
		{
			Vector3[] path = new Vector3[list.Count + (close ? 1 : 0)];

			for( int i = 0; i < list.Count; i++ )
			{
				path[i] = function( list[i] );
			}

			if ( close )
			{
				path[list.Count] = path[0];
			}

			return path;
		}

		/// <summary>
		/// Extract a path from a source.
		/// </summary>
		/// <typeparam name="T">Source type.</typeparam>
		/// <param name="source">Source.</param>
		/// <param name="n">Number of path points. (before closure)</param>
		/// <param name="function">Function.</param>
		/// <param name="close">Whether or not the last point should be a duplicate of the first point.</param>
		/// <returns></returns>
		public static Vector3[] ExtractPath<T>( this T source, int n, Func<T,int,Vector3> function, bool close = false )
		{
			Vector3[] path = new Vector3[n + (close ? 1 : 0)];

			for( int i = 0; i < n; i++ )
			{
				path[i] = function( source, i );
			}

			if ( close )
			{
				path[n] = path[0];
			}

			return path;
		}
	}
}
