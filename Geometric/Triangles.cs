﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace ToBoldlyPlay.Geometric
{
	public static partial class Geometry
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="v0"></param>
		/// <param name="v1"></param>
		/// <param name="v2"></param>
		/// <returns></returns>
		public static Vector3 Normal( Vector3 v0, Vector3 v1, Vector3 v2 )
		{
			return Vector3.Cross( v0 - v1, v1 - v2 ).normalized;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="triangle"></param>
		/// <returns></returns>
		public static Vector3 Normal( this Triangle triangle )
		{
			return Normal( triangle.a, triangle.b, triangle.c );
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="v0"></param>
		/// <param name="v1"></param>
		/// <param name="v2"></param>
		/// <returns></returns>
		public static Vector3 Cross( Vector3 v0, Vector3 v1, Vector3 v2 )
		{
			return Vector3.Cross( v0 - v1, v1 - v2 );
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="v0"></param>
		/// <param name="v1"></param>
		/// <param name="v2"></param>
		/// <returns></returns>
		public static Vector3 Center( Vector3 v0, Vector3 v1, Vector3 v2 )
		{
			return (v0 + v1 + v2) * (1f / 3f);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="triangle"></param>
		/// <returns></returns>
		public static Vector3 Center( this Triangle triangle )
		{
			return Center( triangle.a, triangle.b, triangle.c );
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="v0"></param>
		/// <param name="v1"></param>
		/// <param name="v2"></param>
		/// <param name="point"></param>
		/// <returns></returns>
		public static bool Contains( Vector3 v0, Vector3 v1, Vector3 v2, Vector3 point )
		{
			Vector3 n = Normal( v0, v1, v2 );

			if ( Normal( v0, v1, point ) != n )
			{
				return false;
			}

			if ( Normal( v1, v2, point ) != n )
			{
				return false;
			}

			if ( Normal( v2, v0, point ) != n )
			{
				return false;
			}

			return true;
		}
		
		/// <summary>
		/// Test a point against a triangle using barycentric coordinates.
		/// </summary>
		/// <param name="point">Point.</param>
		/// <param name="triangle">Triangle.</param>
		/// <returns>True, if the triangle contains the point.</returns>
		public static bool PointInTriangle_Barycentric( Vector3 point, Triangle triangle )
		{
			// Compute vectors
			var a = triangle.c - triangle.a;
			var b = triangle.b - triangle.a;
			var c = point - triangle.a;
		
			// Compute dot products
			var dot00 = 1f;
			var dot01 = Vector3.Dot(a, b);
			var dot02 = Vector3.Dot(a, c);
			var dot11 = 1f;
			var dot12 = Vector3.Dot(b, c);
		
			// Compute barycentric coordinates
			var invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
			var u = (dot11 * dot02 - dot01 * dot12) * invDenom;
			var v = (dot00 * dot12 - dot01 * dot02) * invDenom;
		
			// Check if point is in triangle
			return (u >= 0) && (v >= 0) && (u + v < 1);
		}
	
		/// <summary>
		/// Test a point against three planes.
		/// </summary>
		/// <param name="point">Point.</param>
		/// <param name="a">Plane.</param>
		/// <param name="b">Plane.</param>
		/// <param name="c">Plane.</param>
		/// <returns>True, if the triangle defined by the three planes contains the point.</returns>
		public static bool PointInTriangle_ThreePlanes( Vector3 point, Plane a, Plane b, Plane c )
		{			
			return a.GetSide( point ) && b.GetSide( point ) && c.GetSide( point );
		}
	}
}
