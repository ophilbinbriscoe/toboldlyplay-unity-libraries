﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ToBoldlyPlay.Geometric
{
	public static partial class Geometry
	{
		public class Path : IEnumerable<Vector3>
		{
			private Vector3[] points;

			public Path ()
			{
				points = new Vector3[0];
			}

			public Path ( params Vector3[] points )
			{
				this.points = new Vector3[points.Length];
				points.CopyTo( this.points, 0 );
			}

			public Path ( IEnumerable<Vector3> points )
			{
				this.points = points.ToArray();
			}

			public static Path operator - ( Path path )
			{
				return Geometry.Invert( path.points );
			}

			public static Path operator + ( Path path, Vector3 vector )
			{
				return Geometry.Displace( path.points, vector );
			}

			public static Path operator - ( Path path, Vector3 vector )
			{
				return Geometry.Displace( path.points, -vector );
			}

			public static Path operator * ( Path path, float scaleFactor )
			{
				return Geometry.Scale( path.points, scaleFactor );
			}

			public static Path operator * ( Path path, Vector3 scaleVector )
			{
				return Geometry.Scale( path.points, scaleVector );
			}

			public static Path operator * ( Quaternion quaternion, Path path )
			{
				return Geometry.Rotate( path.points, quaternion );
			}

			public static Path operator * ( Matrix4x4 matrix, Path path )
			{
				return Geometry.Transform( path.points, matrix );
			}

			public static implicit operator Vector3[] ( Path path ) { return path.points.Clone() as Vector3[]; }

			public static implicit operator Path ( Vector3[] path ) { return new Path( path ); }

			public static implicit operator Path ( List<Vector3> path ) { return new Path( path ); }

			public IEnumerator<Vector3> GetEnumerator ()
			{
				return ((IEnumerable<Vector3>) points).GetEnumerator();
			}

			IEnumerator IEnumerable.GetEnumerator ()
			{
				return ((IEnumerable<Vector3>) points).GetEnumerator();
			}
		}
	}
}
