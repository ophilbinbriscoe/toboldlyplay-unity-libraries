﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ToBoldlyPlay.Geometric
{
	public static partial class Geometry
    {
        /// <summary>
        /// Take the intersection of two line segments.
        /// </summary>
        /// <param name="segment1">1st segment.</param>
        /// <param name="segment2">2nd segment.</param>
        /// <param name="intersection">Segment of ntersection.</param>
        /// <returns>Outcome of intersection.</returns>
        public static IntersectionOutcome Intersection ( Segment segment1, Segment segment2, out Segment intersection )
        {
            Vector3 u1 = segment1.p2 - segment1.p1; //calculate nonuniform tangents
            Vector3 u2 = segment2.p2 - segment2.p1;

            float dot = Vector3.Dot( u1.normalized, u2.normalized );
            float adot = dot > 0f ? dot : -dot;

            intersection = new Segment( Vector3.zero, Vector3.zero );

            if ( adot >= 0.9999999f )   //parallel
            {
                float[] c1 = new float[2];
                c1[0] = Coefficient( segment1.p1, u1, segment2.p1, u2 );
                c1[1] = Coefficient( segment1.p1, u1, segment2.p2, u2 );

                if ( segment1.p1 + u1 * c1[0] != segment2.p1 )
                    return 0;                                   //disjoint

                bool[] less = new bool[2];
                less[0] = c1[0] <= 1f;
                less[1] = c1[1] <= 1f;

                bool[] greater = new bool[2];
                greater[0] = c1[0] >= 0f;
                greater[1] = c1[1] >= 0f;

                bool[] b = new bool[2];
                b[0] = less[0] && greater[0];
                b[1] = less[1] && greater[1];

                if ( (!less[0] && !less[1]) || (!greater[0] && !greater[1]) )
                    return 0;   //no overlap

                if ( b[0] && b[1] )                                                             //L2 within L1
                {
                    intersection = segment2;
                }
                else if ( b[0] )
                {
                    if ( !less[1] )
                        intersection = new Segment( segment2.p1, segment1.p2 );         //partial overlap 
                    else if ( !greater[1] )
                        intersection = new Segment( segment1.p1, segment2.p1 ); //partial overlap
                }
                else if ( b[1] )
                {
                    if ( !less[0] )
                        intersection = new Segment( segment2.p2, segment1.p2 );         //partial overlap
                    else if ( !greater[0] )
                        intersection = new Segment( segment1.p1, segment2.p2 ); //partial overlap
                }
                else if ( (!greater[0] && !less[1]) || (!greater[1] && !less[0]) )      //L1 within L2
                {
                    intersection = segment1;
                }

                return IntersectionOutcome.LINEAR;
            }
            else
            {
                Vector3 cross = Vector3.Cross( u1, u2 );

                if ( Projection( segment1.p1, segment2.p1, cross, cross ) != segment1.p1 )
                {
                    return 0;       //disjoint, lines lay on seperate planes
                }

                Vector3 n = Vector3.Cross( cross, u2 );

                float c = Coefficient( segment1.p1, u1, segment2.p1, n );

                if ( c < 0f || c > 1f )
                    return 0;                                       //disjoint, intersection is outside of L1

                Vector3 x = segment1.p1 + u1 * c;

                float d1 = Vector3.Dot( x - segment2.p1, u2 );
                float d2 = Vector3.Dot( x - segment2.p2, -u2 );

                if ( d1 < 0f || d2 < 0f )
                    return 0;                                       //disjoint, intersection is outside of L2

                //point intersection

                intersection = new Segment( x, x );

                if ( ((!segment1.b1) && x == segment1.p1) || ((!segment1.b2) && x == segment2.p2) || ((!segment2.b1) && x == segment2.p1) || ((!segment2.b2) && x == segment2.p2) )
                {
                    return IntersectionOutcome.DISJOINT;                                                        //intersects excluded endpoint
                }

                return IntersectionOutcome.POINT;                                                               //disjoint
            }
        }

        /// <summary>
        /// Take the intersection of two infinite lines.
        /// </summary>
        /// <param name="point1">Point along 1st line.</param>
        /// <param name="tangent1">Tangent of 1st line.</param>
        /// <param name="point2">Point along 2nd line.</param>
        /// <param name="tangent2">Tangent of 2nd line.</param>
        /// <param name="intersectionPoint">Point of intersection.</param>
        /// <param name="intersectionTangent">Tangent of intersection.</param>
        /// <returns>Outcome of intersection.</returns>
        public static IntersectionOutcome Intersection ( Vector3 point1, Vector3 tangent1, Vector3 point2, Vector3 tangent2, out Vector3 intersectionPoint, out Vector3 intersectionTangent )
        {
            Vector3 u = Vector3.Cross( tangent1, tangent2 );          // cross product
            float ax = (u.x >= 0 ? u.x : -u.x);
            float ay = (u.y >= 0 ? u.y : -u.y);
            float az = (u.z >= 0 ? u.z : -u.z);

            // test if the two planes are parallel

            if ( (ax + ay + az) < ERROR )       // Pn1 and Pn2 are near parallel
            {
                intersectionPoint = Vector3.zero;
                intersectionTangent = Vector3.zero;

                // test if disjoint or coincide

                Vector3 v = point2 - point1;

                if ( Vector3.Dot( tangent1, v ) == 0f )          // Pn2.V0 lies in Pn1
                {
                    return IntersectionOutcome.POINT;                    // Pn1 and Pn2 coincide
                }
                else
                {
                    return IntersectionOutcome.DISJOINT;                    // Pn1 and Pn2 are disjoint
                }
            }

            // Pn1 and Pn2 intersect in a line
            // first determine max abs coordinate of cross product
            int maxc;                       // max coordinate
            if ( ax > ay )
            {
                if ( ax > az )
                {
                    maxc = 1;
                }
                else
                {
                    maxc = 3;
                }
            }
            else
            {
                if ( ay > az )
                {
                    maxc = 2;
                }
                else
                {
                    maxc = 3;
                }
            }

            // next, to get a point on the intersect line
            // zero the max coord, and solve for the other two

            Vector3 iP;                // intersect point
            float d1, d2;            // the constants in the 2 plane equations

            d1 = -Vector3.Dot( tangent1, point1 );  // note: could be pre-stored  with plane
            d2 = -Vector3.Dot( tangent2, point2 );  // ditto

            switch ( maxc ) // select max coordinate
            {
            case 1:             // intersect with x=0
                iP = new Vector3( 0f, (d2 * tangent1.z - d1 * tangent2.z) / u.x, (d1 * tangent2.y - d2 * tangent1.y) / u.x );
                break;
            case 2:             // intersect with y=0
                iP = new Vector3( (d1 * tangent2.z - d2 * tangent1.z) / u.y, 0f, (d2 * tangent1.x - d1 * tangent2.x) / u.y );
                break;
            case 3:             // intersect with z=0
                iP = new Vector3( (d2 * tangent1.y - d1 * tangent2.y) / u.z, (d1 * tangent2.x - d2 * tangent1.x) / u.z, 0f );
                break;
            default:
                iP = Vector3.zero;
                break;
            }

            intersectionPoint = iP;
            intersectionTangent = u;

            return IntersectionOutcome.LINEAR;
        }

        /// <summary>
        /// Take the intersection of a triangle and an unbounded plane.
        /// </summary>
        /// <param name="triangle">Triangle.</param>
        /// <param name="planePoint">Plane point.</param>
        /// <param name="planeNormal">Plane normal.</param>
        /// <param name="intersection">Intersection.</param>
        /// <returns>Outcome of intersection.</returns>
        public static IntersectionOutcome Intersection ( Triangle triangle, Vector3 planePoint, Vector3 planeNormal, out Segment intersection )
        {
            Vector3[] d = new Vector3[3];

            Vector3[] v = new Vector3[] { triangle.a, triangle.b, triangle.c };

            d[0] = triangle.b - triangle.a;
            d[1] = triangle.c - triangle.b;
            d[2] = triangle.a - triangle.c;

            float[] c = new float[3];

            c[0] = Coefficient( triangle.a, d[0], planePoint, planeNormal );
            c[1] = Coefficient( triangle.b, d[1], planePoint, planeNormal );
            c[2] = Coefficient( triangle.c, d[2], planePoint, planeNormal );

            Vector3[] x = new Vector3[2];
            Vector3 p = Vector3.zero;

            int xi = 0; //counter for line intersections
            int pi = 0; //counter for point intersections

            for ( int i = 0; i < 3; i++ )
            {
                if ( c[i] > 0f && c[i] < 1f )
                {
                    x[xi] = v[i] + d[i] * c[i];
                    xi++;
                    if ( xi == 2 )
                        break;
                }
                else if ( c[i] == 0f )
                {
                    p = triangle.a;
                    pi++;
                    break;
                }
            }

            if ( pi == 1 )
            {
                intersection = new Segment( p, p );
                return IntersectionOutcome.POINT;
            }
            else if ( xi == 2 )
            {
                intersection = new Segment( x[0], x[1] );
                return IntersectionOutcome.LINEAR;
            }

            intersection = new Segment( Vector3.zero, Vector3.zero );

            return IntersectionOutcome.DISJOINT;
        }
    }
}
