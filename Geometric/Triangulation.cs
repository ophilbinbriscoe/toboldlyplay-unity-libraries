﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using ToBoldlyPlay.Functional;

namespace ToBoldlyPlay.Geometric
{
	/// <summary>
	/// 
	/// </summary>
	public static class Triangulation
	{
		public delegate bool SideEffect ( int v0, int v1, int v2 );

		public delegate void Function ( IEnumerable<Vector3> points, IList<int> indices, SideEffect sideEffect );

		/// <summary>
		/// Execute a triangulation function on a path.
		/// </summary>
		/// <param name="path">Path.</param>
		/// <param name="function">Function.</param>
		/// <returns>Array of triangles.</returns>
		public static Triangle[] Triangles ( Vector3[] path, Function function )
		{
			if ( path.Length < 3 )
				return new Triangle[] { };

			var triangles = new List<Triangle>();

			function.Invoke( path, path.Indices(), ( x, y, z ) => { triangles.Add( new Triangle( path[x], path[y], path[z] ) ); return false; } );

			return triangles.ToArray();
		}

		/// <summary>
		/// Execute a triangulation function on a set of points. Unity / Mesh friendly.
		/// </summary>
		/// <param name="points">Points.</param>
		/// <param name="indices">Order of points along path.</param>
		/// <param name="function">Function.</param>
		/// <returns>Triangle indices.</returns>
		public static int[] Triangles ( IEnumerable<Vector3> points, IList<int> indices, Function function )
		{
			if ( indices.Count < 3 || points.Count() < 3 )
			{
				return new int[] { };
			}

			var triangles = new List<int>();

			function( points, indices, ( x, y, z ) => { triangles.AddRange( new int[] { x, y, z } ); return false; } );

			return triangles.ToArray();
		}

		/// <summary>
		/// Naive triangulation function, suitable for solid polygons with convex corners.
		/// </summary>
		/// <param name="points">Points.</param>
		/// <param name="indices">Order of points along path.</param>
		/// <param name="sideEffect">Side effect.</param>
		public static void Naive ( IEnumerable<Vector3> points, IList<int> indices, SideEffect sideEffect )
		{
			if ( indices.Count < 3 || points.Count() < 3 )
				return;

			bool closed = points.ElementAt( indices[0] ) == points.ElementAt( indices[indices.Count - 1] );

			for ( int i = 2; i < (closed ? indices.Count - 1 : indices.Count); i++ )
			{
				if ( sideEffect.Invoke( 0, i - 1, i ) )
				{
					return;
				}
			}
		}

		public static void Dweyer ( IEnumerable<Vector3> points, IList<int> indices, SideEffect sideEffect )
		{
			var normal = new Triangle( points.ElementAt( 0 ), points.ElementAt( 1 ), points.ElementAt( 2 ) ).Normal();

			var projection = new List<Vector3>( points );

			for ( int i = 0; i < projection.Count; i++ )
			{
				projection[i] = Geometry.Projection( points.First(), normal, projection[i], normal );
			}


		}

		/// <summary>
		/// Ear-clipping triangulation function, suitable for solid polygons with convex and concave corners.
		/// </summary>
		/// <param name="points">Points.</param>
		/// <param name="indices">Order of points along path.</param>
		/// <param name="sideEffect">Side effect.</param>
		public static void EarClip ( IEnumerable<Vector3> points, IList<int> indices, SideEffect sideEffect )
		{
			if ( indices.Count < 3 || points.Count() < 3 )
			{
				return;
			}

			var array = points.ToArray();

			var polygon = new List<Vector3>();

			foreach ( int i in indices )
			{
				polygon.Add( array[i] );
			}

			Vector3[] path = polygon.ToArray();
			Vector3 normal = path.Normal();

			//			Debug.Draw( path, Color.yellow, Color.red ); //debug

			int s = 0, cnt = indices.Count, t = 0, p = 0; //debug

			int a = 0, b = 1, c = 2;

			bool debug = false;

			while ( indices.Count > 2 )
			{
				if ( s++ >= 2000 )
				{
					throw new System.InvalidProgramException( "Ear-clip triangulation loop exhausted" );
				}

				for ( int i = 0; i < indices.Count; i++ )
				{
					a = (a + 1) % indices.Count;
					b = (b + 1) % indices.Count;
					c = (c + 1) % indices.Count;

					Triangle triangle = new Triangle( array[indices[a]], array[indices[b]], array[indices[c]] );

					//Check for colinearity

					if ( Geometry.Colinear( triangle.b - triangle.a, triangle.c - triangle.b ) )
					{
						continue;
					}

					//Check for concavity

					Vector3 n = triangle.Normal();

					if ( Vector3.Dot( n, normal ) < 0 )
					{
						continue;
					}

					//Check for intersections

					Plane	pa = new Plane( Vector3.Cross( triangle.a - triangle.b, n ).normalized, triangle.a ),
							pb = new Plane( Vector3.Cross( triangle.b - triangle.c, n ).normalized, triangle.b ),
							pc = new Plane( Vector3.Cross( triangle.c - triangle.a, n ).normalized, triangle.c );

					bool test = false;

					for ( int m = 0; m < indices.Count; m++ )
					{
						if ( m == a || m == b || m == c )
							continue;

						if ( Geometry.PointInTriangle_ThreePlanes( array[indices[m]], pa, pb, pc ) )
						{
							test = true;

							if ( debug )
							{
								Debug.Log( "\t\tIntersection." );
								Debug.DrawLine( triangle.a + normal * p * 5f, triangle.c + normal * p * 5f, Color.red );
							}

							break;
						}
					}
					if ( test )
						continue;

					//Clip vertex

					if ( sideEffect.Invoke( indices[a], indices[b], indices[c] ) )
					{
						return;
					}

					indices.RemoveAt( b );

					if ( debug )
					{
						(triangle + normal * p * 5f).DrawDebug( Color.green );

						t++;

						Debug.Log( "\t\tClipping Vertex: " + b );
					}

					a = a % indices.Count;
					b = a + 1;
					c = a + 2;

					break;
				}
			}

			if ( s >= 200 )
			{
				Debug.Log( "Escape." );
			}

			if ( debug )
			{
				Debug.Log( "In-Points: " + (cnt) + "Out-Tris: " + t );
				Debug.Break();
			}
		}
	}
}
