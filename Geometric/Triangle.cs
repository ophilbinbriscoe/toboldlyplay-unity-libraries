﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ToBoldlyPlay.Geometric
{
	public class Triangle<T> : IEnumerable<T>
	{
		[SerializeField] protected T[] corners;

		public T this[int i]
		{
			get
			{
				return corners[i];
			}

			set
			{
				corners[i] = value;
			}
		}

		/// <summary>
		/// Triangle corner.
		/// </summary>
		public T a
		{
			get { return corners[0]; }
			set { corners[0] = value; }
		}

		/// <summary>
		/// Triangle corner.
		/// </summary>
		public T b
		{
			get { return corners[1]; }
			set { corners[0] = value; }
		}

		/// <summary>
		/// Triangle corner.
		/// </summary>
		public T c
		{
			get { return corners[2]; }
			set { corners[0] = value; }
		}

		/// <summary>
		/// Create a Triangle from its three corners.
		/// </summary>
		/// <param name="v0">1st corner.</param>
		/// <param name="v1">2nd corner.</param>
		/// <param name="v2">3rd corner.</param>
		public Triangle( T a, T b, T c )
		{
			corners = new T[] { a, b, c };
		}
		
		/// <summary>
		/// Create a copy of a Triangle.
		/// </summary>
		/// <param name="triangle">Triangle to copy.</param>
		public Triangle ( Triangle<T> triangle )
		{
			corners = new T[] { triangle.corners[0], triangle.corners[1], triangle.corners[2] };
		}

		/// <summary>
		/// Create a Triangle using the next three points in an enumeration.
		/// </summary>
		/// <param name="enumerator">Enumerator.</param>
		public Triangle( IEnumerator<T> enumerator )
		{
			corners = new T[3];
			
			for( int i = 0; i < 3; i++ )
			{
				if ( !enumerator.MoveNext() )
				{
					throw new InvalidOperationException( "Reached end of enumeration before Triangle could be completed." );
				}

				corners[i] = enumerator.Current;
			}
		}

		public IEnumerator<T> GetEnumerator ()
		{
			return ((IEnumerable<T>) corners).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return ((IEnumerable<T>) corners).GetEnumerator();
		}
	}

	[Serializable]
	public class Triangle : Triangle<Vector3>
	{
		/// </summary>
		/// <param name="v0">0th corner.</param>
		/// <param name="v1">1st corner.</param>
		/// <param name="v2">2nd corner.</param>
		public Triangle ( Vector3 a, Vector3 b, Vector3 c ) : base( a, b, c ) { }

		/// <summary>
		/// Create a triangle using the next three points yielded by an enumerator.
		/// </summary>
		/// <param name="enumerator">Enumerator.</param>
		public Triangle ( IEnumerator<Vector3> enumerator ) : base( enumerator ) { }

		/// <summary>
		/// Invert winding order.
		/// </summary>
		/// <param name="triangle">Input triangle.</param>
		/// <returns>Input triangle, with winding order reversed.</returns>
		public static Triangle operator - ( Triangle triangle )
		{
			return new Triangle( triangle.c, triangle.b, triangle.a );
		}

		/// <summary>
		/// Displace triangle.
		/// </summary>
		/// <param name="triangle">Input triangle.</param>
		/// <param name="vector">Displacement vector.</param>
		/// <returns>Input triangle, displaced along the displacement vector.</returns>
		public static Triangle operator + ( Triangle triangle, Vector3 vector )
		{
			return new Triangle( triangle.a + vector, triangle.b + vector, triangle.c + vector );
		}

		/// <summary>
		/// Displace triangle.
		/// </summary>
		/// <param name="triangle">Input triangle.</param>
		/// <param name="vector">Displacement vector.</param>
		/// <returns>Input triangle, displaced along the negative of the displacement vector.</returns>
		public static Triangle operator - ( Triangle triangle, Vector3 vector )
		{
			return new Triangle( triangle.a - vector, triangle.b - vector, triangle.c - vector );
		}

		/// <summary>
		/// Scale triangle.
		/// </summary>
		/// <param name="triangle">Input triangle.</param>
		/// <param name="f">Scale factor.</param>
		/// <returns>Input triangle, scaled by the scale factor.</returns>
		public static Triangle operator * ( Triangle triangle, float f )
		{
			return new Triangle( triangle.a * f, triangle.b * f, triangle.c * f );
		}

		/// <summary>
		/// Rotate triangle.
		/// </summary>
		/// <param name="quaternion">Quaternion.</param>
		/// <param name="triangle">Input triangle.</param>
		/// <returns>Input triangle, rotated by the quaternion.</returns>
		public static Triangle operator * ( Quaternion quaternion, Triangle triangle )
		{
			return new Triangle( quaternion * triangle.a, quaternion * triangle.b, quaternion * triangle.c );
		}
		
		/// <summary>
		/// Transform triangle.
		/// </summary>
		/// <param name="matrix">Transformation matrix.</param>
		/// <param name="triangle">Input triangle.</param>
		/// <returns>Input triangle, transformed by the transformation matrix.</returns>
		public static Triangle operator *(Matrix4x4 matrix, Triangle triangle)
		{
			return new Triangle( matrix.MultiplyPoint3x4( triangle.a ), matrix.MultiplyPoint3x4( triangle.b ), matrix.MultiplyPoint3x4( triangle.c ) );
		}
	}
}
