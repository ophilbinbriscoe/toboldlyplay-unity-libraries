﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ToBoldlyPlay.Geometric
{
	public static class DebugExtensions
	{
		/// <summary>
		/// Draw a triangle.
		/// </summary>
		/// <param name="triangle">Triangle to draw.</param>
		public static void DrawDebug( this Triangle triangle )
		{
			DrawDebug ( triangle, Color.white );
		}

		/// <summary>
		/// Draw a Triangle.
		/// </summary>
		/// <param name="triangle">Triangle to draw.</param>
		/// <param name="color">Color to draw with.</param>
		public static void DrawDebug( this Triangle triangle, Color color )
		{
			Debug.DrawLine( triangle.a, triangle.b, color );
			Debug.DrawLine( triangle.b, triangle.c, color );
			Debug.DrawLine( triangle.c, triangle.a, color );
		}

		/// <summary>
		/// Draw a Path.
		/// </summary>
		/// <param name="path">Path to draw.</param>
		public static void DrawDebug( this IList<Vector3> path )
		{
			DrawDebug ( path, Color.white );	
		}
		
		/// <summary>
		/// Draw a Path.
		/// </summary>
		/// <param name="path">Path to draw.</param>
		/// <param name="color"></param>
		public static void DrawDebug( this IList<Vector3> path, Color color )
		{
			for( int i = 1; i < path.Count; i++ )
			{
				Debug.DrawLine ( path[i], path[i-1], color );			
			}
		}
		
		/// <summary>
		/// Draw a Path with a color gradient.
		/// </summary>
		/// <param name="path">Path to draw.</param>
		/// <param name="startColor">Color at path start.</param>
		/// <param name="endColor">Color at path end.</param>
		public static void DrawDebug( this IList<Vector3> path, Color startColor, Color endColor )
		{
			for ( int i = 1; i < path.Count; i++ )
			{
				Debug.DrawLine ( path[i], path[i-1], Color.Lerp ( startColor, endColor, ( i / (path.Count - 1f) ) ) );			
			}
		}

		/// <summary>
		/// Draw a Segment.
		/// </summary>
		/// <param name="segment">Segment to draw.</param>
		public static void DrawDebug( this Segment segment )
		{
			Debug.DrawLine( segment.p1, segment.p2, Color.white );
		}
		
		/// <summary>
		/// Draw a Segment.
		/// </summary>
		/// <param name="segment">Segment to draw.</param>
		/// <param name="color">Color to draw with.</param>
		public static void DrawDebug( this Segment segment, Color color )
		{
			Debug.DrawLine( segment.p1, segment.p2, color );
		}

		/// <summary>
		/// Draw a Path.
		/// </summary>
		/// <param name="path">Path to draw.</param>
		public static void DrawDebug<T>( this IList<T> path, Func<T,Vector3> function, bool close = false )
		{
			DrawDebug( path, function, Color.white, close );
		}

		/// <summary>
		/// Draw a Path.
		/// </summary>
		/// <param name="path">Path to draw.</param>
		public static void DrawDebug<T>( this IList<T> path, Func<T,Vector3> function, Color color, bool close = false )
		{
			for( int i = 1; i < path.Count; i++ )
			{
				Debug.DrawLine( function( path[i] ), function( path[i - 1] ), color );
			}

			if ( close )
			{
				Debug.DrawLine( function( path[path.Count - 1] ), function( path[0] ), color );
			}
		}
	}
}
