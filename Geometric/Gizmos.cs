﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ToBoldlyPlay.Geometric
{
	public static class GizmoExtensions
	{
		/// <summary>
		/// Draw a triangle.
		/// </summary>
		/// <param name="triangle">Triangle to draw.</param>
		public static void DrawGizmo( this Triangle triangle )
		{
			Gizmos.DrawLine( triangle.a, triangle.b );
			Gizmos.DrawLine( triangle.b, triangle.c );
			Gizmos.DrawLine( triangle.c, triangle.a );
		}

		/// <summary>
		/// Draw a Path.
		/// </summary>
		/// <param name="path">Path to draw.</param>
		public static void DrawGizmo( this IList<Vector3> path )
		{
			for( int i = 1; i < path.Count; i++ )
			{
				Gizmos.DrawLine ( path[i], path[i-1] );			
			}
		}

		/// <summary>
		/// Draw a Segment.
		/// </summary>
		/// <param name="segment">Segment to draw.</param>
		public static void DrawGizmo( this Segment segment )
		{
			Gizmos.DrawLine( segment.p1, segment.p2 );
		}

		/// <summary>
		/// Draw a Path.
		/// </summary>
		/// <param name="path">Path to draw.</param>
		public static void DrawGizmo<T>( this IList<T> path, Func<T,Vector3> function, bool close = false )
		{
			for( int i = 1; i < path.Count; i++ )
			{
				Gizmos.DrawLine( function( path[i] ), function( path[i - 1] ) );
			}

			if ( close )
			{
				Gizmos.DrawLine( function( path[path.Count - 1] ), function( path[0] ) );
			}
		}
	}
}
