﻿using System.Collections.Generic;
using UnityEngine;

namespace ToBoldlyPlay.Geometric
{
	public static partial class Geometry
	{
		public static Vector3 TryConform ( Vector3 a, Vector3 b )
		{
			if ( Compare( a, b ) )
				return b;
			return a;
		}

		private static Vector3 TryConform ( Vector3 a, IList<Vector3> list )
		{
			if ( ERROR == 0f )
				return a;

			foreach ( Vector3 b in list )
				if ( Vector3.SqrMagnitude( a - b ) <= ERROR ) return b;
			return a;
		}

		public static bool Compare ( Vector3 a, Vector3 b )
		{
			return ERROR == 0f ? a == b : Vector3.Distance( a, b ) <= ERROR;
		}

		public static bool Compare ( Vector3 a, IList<Vector3> list )
		{
			if ( ERROR == 0f )
			{
				foreach ( Vector3 b in list )
					if ( a == b ) return true;
				return false;
			}

			foreach ( Vector3 b in list )
				if ( Vector3.SqrMagnitude( a - b ) <= ERROR ) return true;
			return false;
		}

		public static bool Colinear ( Vector3 a, Vector3 b )
		{
			if ( b == Vector3.zero ) return false;
			Vector3 c = new Vector3( Mathf.Abs( b.x ), Mathf.Abs( b.y ), Mathf.Abs( b.z ) );
			int max = c.x > c.y ? (c.x > c.z ? 0 : 2) : (c.y > c.z ? 1 : 2);
			switch ( max )
			{
			case 0: return Compare( a, b * (a.x / b.x) );
			case 1: return Compare( a, b * (a.y / b.y) );
			case 2: return Compare( a, b * (a.z / b.z) );
			}
			return false;
		}
	}
}
