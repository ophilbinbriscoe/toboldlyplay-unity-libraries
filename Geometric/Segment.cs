﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ToBoldlyPlay.Geometric
{
	[Serializable]
	public class Segment : IEnumerable<Vector3>
	{
		[SerializeField] private Vector3[] endpoints;

		/// <summary>
		/// Endpoint.
		/// </summary>
		public Vector3 p1
		{
			get
			{
				return endpoints[0];
			}

			set
			{
				endpoints[1] = value;
			}
		}

		/// <summary>
		/// Endpoint.
		/// </summary>
		public Vector3 p2
		{
			get
			{
				return endpoints[1];
			}

			set
			{
				endpoints[1] = value;
			}
		}

		/// <summary>
		/// True, if segment includes 1st endpoint.
		/// </summary>
		public bool b1;

		/// <summary>
		/// True, if segment includes 2nd endpoint.
		/// </summary>
		public bool b2;
		
		/// <summary>
		/// Create a line segment.
		/// </summary>
		/// <param name="p1">1st endpoint.</param>
		/// <param name="p2">2nd endpoint.</param>
		public Segment( Vector3 p1, Vector3 p2 )
		{
			endpoints = new Vector3[] { p1, p2 };
			b1 = true;
			b2 = true;
		}
		
		/// <summary>
		/// Create a line segment.
		/// </summary>
		/// <param name="p1">1st endpoint.</param>
		/// <param name="p2">2nd endpoint.</param>
		/// <param name="b1">True, if segment includes 1st endpoint.</param>
		/// <param name="b2">True, if segment includes 2nd endpoint.</param>
		public Segment( Vector3 p1, Vector3 p2, bool b1, bool b2 )
		{
			endpoints = new Vector3[] { p1, p2 };
			this.b1 = b1;
			this.b2 = b2;
		}

		public IEnumerator<Vector3> GetEnumerator ()
		{
			return ((IEnumerable<Vector3>) endpoints).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return ((IEnumerable<Vector3>) endpoints).GetEnumerator();
		}
	}
}
