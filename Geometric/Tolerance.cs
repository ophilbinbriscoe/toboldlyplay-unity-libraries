﻿namespace ToBoldlyPlay.Geometric
{
	public static partial class Geometry
	{
		private const float DEFAULT_ERROR = 0f;

		private static float ERROR;

		/// <summary>
		/// Floating point tolerance.
		/// </summary>
		public static float Error
		{
			get { return ERROR; }
			set { ERROR = value; }
		}

		/// <summary>
		/// Define a new floating point tolerance.
		/// </summary>
		/// <param name="error"></param>
		public static void SetError( float error ) { ERROR = error; }

		/// <summary>
		/// Restore floating point tolerance to default.
		/// </summary>
		public static void ResetError() { ERROR = DEFAULT_ERROR; }
	}
}
