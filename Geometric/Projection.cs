﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace ToBoldlyPlay.Geometric
{
	public static partial class Geometry
	{	
		/// <summary>
		/// Porjection coefficient for projecting a point onto a plane.
		/// </summary>
		/// <param name="planePoint">Plane point.</param>
		/// <param name="planeNormal">Plane normal.</param>
		/// <param name="point">Point to project.</param>
		/// <param name="projection">Projection vector.</param>
		/// <returns></returns>
		public static float Coefficient ( Vector3 planePoint, Vector3 planeNormal, Vector3 point, Vector3 projection )
		{
			return - ( ( Vector3.Dot ( planeNormal, point ) - Vector3.Dot ( planeNormal, planePoint ) ) / Vector3.Dot ( planeNormal, projection ) );
		}

		/// <summary>
		/// Project a point onto a plane.
		/// </summary>
		/// <param name="planePoint">Plane point.</param>
		/// <param name="planeNormal">Plane normal.</param>
		/// <param name="point">Point to project.</param>
		/// <param name="projection">Projection vector.</param>
		/// <returns>Projected point.</returns>
		public static Vector3 Projection ( Vector3 planePoint, Vector3 planeNormal, Vector3 point, Vector3 projection )
		{
			return point + projection * - ( ( Vector3.Dot ( planeNormal, point ) - Vector3.Dot ( planeNormal, planePoint ) ) / Vector3.Dot ( planeNormal, projection ) );
		}
	
		/// <summary>
		/// Project a path onto a plane.
		/// </summary>
		/// <param name="planePoint">Plane point.</param>
		/// <param name="planeNormal">Plane normal.</param>
		/// <param name="path">Path to project.</param>
		/// <param name="projection">Projection vector.</param>
		/// <returns>Projected path.</returns>
		public static Vector3[] Projection ( Vector3 planePoint, Vector3 planeNormal, Vector3[] path, Vector3 projection )
		{	
			float a = Vector3.Dot ( planeNormal, planePoint );
			float b = Vector3.Dot ( planeNormal, projection );

			Vector3[] v = new Vector3[path.Length];
		
			for( int i = 0; i < v.Length; i++ )
				v[i] = path[i] + projection * - ( ( Vector3.Dot( planeNormal, path[i] ) - a ) / b );
		
			return v; 
		}
		
		/// <summary>
		/// Reflect a point about a plane.
		/// </summary>
		/// <param name="point">Point.</param>
		/// <param name="plane">Plane.</param>
		/// <returns>Reflected point.</returns>
		public static Vector3 Reflection( Vector3 point, Plane plane )
		{		
			return ( point + 2 * plane.normal * - plane.GetDistanceToPoint( point ) );
		}
	}
}
