﻿using System.Linq;
using UnityEngine;

namespace ToBoldlyPlay.Geometric
{
	/// <summary>
	/// Class allowing for the efficient traversal of a Path.
	/// All stateless data is cached at instantiation, as such it is best used in cases where speed is more valuable than low memory usage.
	/// </summary>
	public class PathEnumerator
	{
		public Vector3[] Path { get { return path.Clone() as Vector3[]; } }

		private readonly Vector3[] path;
	
		private readonly bool closed;
	
		private readonly float[] lengths;
	
		private readonly Vector3[] pointTangents;
	
		private readonly Vector3[] segmentTangents;
	
		private readonly Vector3[] pointInsets;
	
		private readonly Vector3[] pointInsetsNormalized;
	
		private readonly Vector3[] segmentInsets;
	
		private readonly float[] ts;
	
		private readonly int shortCount;
	
		private readonly int longCount;

		private readonly float area;
	
		//current index along the point array
		private int index;

		private float t;
		
		//number of loops around the path the enumerator has travelled
		//private int loops;
	
		//current point
		private Vector3 point;
	
		//current tangent
		private Vector3 tangent;
		
		/// <summary>
		/// Create a new path enumerator.
		/// </summary>
		/// <param name="path">Path to traverse.</param>
		public PathEnumerator( Vector3[] path ) : this ( path, path.Normal() ) {}
		
		/// <summary>
		/// Create a new path enumerator.
		/// </summary>
		/// <param name="path">Path to traverse.</param>
		/// <param name="normal">Alternative normal vector. Used in calculation of derivative data.</param>
		public PathEnumerator( Vector3[] path, Vector3 normal )
		{	
			this.path = path;
		
			closed = path.IsClosed();
		
			longCount = path.Count();
		
			shortCount = longCount - 1;
	
			lengths = new float[ shortCount ];
		
			pointTangents = new Vector3[ longCount ];
		
			segmentTangents = new Vector3[ shortCount ];
		
			pointInsets = new Vector3[ longCount ];
		
			pointInsetsNormalized = new Vector3[ longCount ];
		
			segmentInsets = new Vector3[ shortCount ];
		
			ts = new float[ longCount ];
		
			ts[0] = 0f;
		
			for ( index = 1; index < longCount; index++ )
			{
				lengths[index-1] = Vector3.Distance ( path[index-1], path[index] );
			
				ts[index] = ts[index-1] + lengths[index-1];
			
				segmentTangents[index-1] = ( path[index] - path[index-1] ).normalized;
			
				segmentInsets[index-1] = Vector3.Cross ( segmentTangents[index - 1], normal ).normalized;
			
				if ( index == shortCount )
				{
					if ( ! closed ) pointTangents[index] = ( path[index] - path[index-1] ).normalized;
					else pointTangents[index] = Vector3.Lerp ( ( path[1] - path[0]).normalized, ( path[index] - path[index-1] ).normalized, 0.5f ).normalized;
				}
				else pointTangents[index] = Vector3.Lerp ( ( path[Next(true)] - path[index]).normalized, ( path[index] - path[index-1] ).normalized, 0.5f ).normalized;
		
				pointInsets[index] = Geometry.Projection( path[index], pointTangents[index], path[index - 1] + segmentInsets[index - 1], segmentTangents[index - 1] ) - path[index];
			
				pointInsetsNormalized[index] = pointInsets[index].normalized;
			}
		
			pointTangents[0] = closed ? pointTangents[ shortCount ] : ( path[1] - path[0] ).normalized;
		
			pointInsets[0] = closed ? pointInsets[ shortCount ] : Vector3.Cross ( pointTangents[0], normal );
		
			pointInsetsNormalized[0] = closed ? pointInsets[0].normalized : pointInsets[0];
		
			index = 0;
		
			point = path.First();
		
			tangent = pointTangents[0];

			area = Geometry.SignedArea( path, normal );
		}
	
		/// <summary>
		/// The line segment on which the enumerator currently lies.
		/// </summary>
		/// <returns>Line segment.</returns>
		public Segment LineSegment() { return LineSegment( false ); }
	
		/// <summary>
		/// The line segment 
		/// </summary>
		/// <param name="wrap">Wheter to allow wrapping. If wrapping is disabled and the enumerator has reached the end of the path, this
		/// will return the last segment along the path. If wrapping is enabled, it will return the first segment.</param>
		/// <returns>Line segment.</returns>
		public Segment LineSegment( bool wrap )
		{
			if ( index == shortCount && (! (wrap && closed) ) ) return new Segment ( path[Previous()], path[index] );
			else return new Segment ( path[index], path[Next()] );
		}
	
		/// <summary>
		/// Get the path's open / closed state.
		/// </summary>
		/// <returns>True, if the first point equals the last.</returns>
		public bool Closed() { return closed; }
	
		/// <summary>
		/// Get the path's length.
		/// </summary>
		/// <returns></returns>
		public float Length() { return ts[ shortCount ]; }
	
		/// <summary>
		/// Get the current displacement along the path.
		/// </summary>
		/// <returns>Displacement.</returns>
		public float Displacement() { return ts[ index ] + t; }
	
		/// <summary>
		/// Get the vector perpendicular to the path plane's normal and the segment on which the enumerator currently lies.
		/// </summary>
		/// <returns>Vector.</returns>
		public Vector3 Inset() { return Inset( false ); }

		/// <summary>
		/// Get the vector perpendicular to the path plane's normal and the segment on which the enumerator currently lies.
		/// </summary>
		/// <param name="normalized">Whether or not to normalize the inset vector before returning it.</param>
		/// <returns>Vector.</returns>
		public Vector3 Inset ( bool normalized )
		{
			if ( t == 0f )
			{
				return normalized ? pointInsets[index] : pointInsets[index].normalized;
			}
			else
			{
				return normalized ? segmentInsets[index] : pointInsets[index].normalized;
			}
		}

		/// <summary>
		/// Get the tangent vector along the current segment on which the enumerator currently lies.
		/// </summary>
		/// <returns>Vector.</returns>
		public Vector3 Tangent()
		{
			if ( t == 0f )
			{
				return pointTangents[index];
			}
			else
			{
				return segmentTangents[index];
			}
		}

		private void CalcPoint ()
		{
			if ( t == 0f )
			{
				point = path[index];
			}
			else
			{
				point = Vector3.Lerp( path[index], path[index + 1], t / (ts[index + 1] - ts[index]) );
			}
		}
	
		/// <summary>
		/// Get the current point along the path.
		/// </summary>
		/// <returns>Point.</returns>
		public Vector3 Point() { return point; }
	
		/// <summary>
		/// Displace the enumerator along the path by a certain amount.
		/// </summary>
		/// <param name="amount">Amount to move.</param>
		public void Crawl ( float amount ) { Crawl ( amount, false ); }
	
		/// <summary>
		/// Displace the enumerator along the path by a certain amount.
		/// </summary>
		/// <param name="amount">Amount to move.</param>
		/// <param name="wrap">Whether or not the enumerator should loop around once it has reached the end.</param>
		public void Crawl( float amount, bool wrap )
		{
			MoveTo ( t + amount );
		}
	
		/// <summary>
		/// Jump to the point along the path at a given vertex index.
		/// </summary>
		/// <param name="i">Vertex index.</param>
		public void MoveTo ( int i ) { MoveTo ( i, false ); }
	
		/// <summary>
		/// Jump to the point along the path at a given vertex index.
		/// </summary>
		/// <param name="i">Vertex index.</param>
		/// <param name="wrap">Whether or not the enumerator should loop around once it has reached the end.</param>
		public void MoveTo ( int i, bool wrap )
		{
			if ( wrap )
			{
				if ( i > 0 ) i %= longCount;
				else while ( i < 0 ) i += longCount;
			}
			else index = Mathf.Clamp ( i, 0, path.Length - 1 );
		
			t = 0f;
			point = path[ index ];
		}
	
		/// <summary>
		/// Jump to the point along the path at a given displacement.
		/// </summary>
		/// <param name="t">Displacement.</param>
		public void MoveTo ( float t ) { MoveTo ( t, false ); }
	
		/// <summary>
		/// Jump to the point along the path at a given displacement.
		/// </summary>
		/// <param name="t">Displacement.</param>
		/// <param name="wrap">Whether or not the enumerator should loop around once it has reached the end.</param>
		public void MoveTo ( float t, bool wrap )
		{
			if ( wrap )
			{
				if ( t > 0f ) while ( t >= ts[shortCount] ) t -= ts[shortCount];
				else while ( t < 0f ) t += ts[shortCount];
			}
			else
			{
				if ( t <= 0f )
				{
					index = 0;
					this.t = 0f;
					CalcPoint();
					return;
				}
			
				if ( t >= ts[ shortCount ] )
				{
					index = shortCount;
					this.t = 0f;
					CalcPoint();
					return;
				}
			}
		
			for ( int i = 0; i < shortCount; i++ )
			{
				if ( ts[i + 1] > t )
				{
					index = i;
					this.t = t - ts[i];
					CalcPoint();
					return;
				} 
			}
		}
	
		/// <summary>
		/// Jump to the next vertex along the path.
		/// </summary>
		public void Forward() { Forward ( false ); }
	
		/// <summary>
		/// Jump to the next vertex along the path.
		/// </summary>
		/// <param name="wrap">Whether or not the enumerator should loop around once it has reached the end.</param>
		public void Forward ( bool wrap )
		{
			if ( HasNext( wrap ) ) MoveTo (	Next( wrap ) );
		}
	
		/// <summary>
		/// Jump to the previous vertex along the path.
		/// </summary>
		public void Backward() { Backward ( false ); }
	
		/// <summary>
		/// Jump to the previous vertex along the path.
		/// </summary>
		/// <param name="wrap">Whether or not the enumerator should loop around once it has reached the start.</param>
		public void Backward ( bool wrap )
		{
			if ( t > 0f ) t = 0f;
			else if ( HasPrevious( wrap ) ) MoveTo ( Previous( wrap ) );
		}
	
		/// <summary>
		/// The index of the most recently traversed vertex along the path.
		/// </summary>
		/// <returns>Index.</returns>
		public int Current() { return index; }
	
		/// <summary>
		/// Test whether or not a next vertex exists.
		/// </summary>
		/// <returns>True, if a forward jump is possible.</returns>
		public bool HasNext() { return HasNext ( false ); }
	
		/// <summary>
		/// Test whether or not a next vertex exists.
		/// </summary>
		/// <param name="wrap">Whether or not to consider points beyond the end of the path.</param>
		/// <returns>True, if a forward jump is possible.</returns>
		public bool HasNext( bool wrap ) { return index < shortCount || ( wrap && path.IsClosed() ); }
	
		/// <summary>
		/// Test whether or not a previous vertex exists.
		/// </summary>
		/// <returns>True, if a backward jump is possible.</returns>
		public bool HasPrevious() { return HasPrevious ( false ); }
	
		/// <summary>
		/// Test whether or not a previous vertex exists.
		/// </summary>
		/// <param name="wrap">Whether or not to consider points beyond the start of the path.</param>
		/// <returns>True, if a backward jump is possible.</returns>
		public bool HasPrevious( bool wrap ) { return index > 0 || ( wrap && path.IsClosed() ); }
	
		/// <summary>
		/// The index of the next vertex along the path.
		/// </summary>
		/// <returns>Index.</returns>
		public int Next() { return Next ( false ); }
	
		/// <summary>
		/// The index of the next vertex along the path.
		/// </summary>
		/// <param name="wrap">Whether or not to consider points beyond the end of the path.</param>
		/// <returns>Index.</returns>
		public int Next ( bool wrap )
		{
			if ( HasNext( wrap ) ) return index == shortCount ? 1 : index + 1;
		
			return index;
		}
	
		/// <summary>
		/// The index of the previous vertex along the path.
		/// </summary>
		/// <returns>Index.</returns>
		public int Previous() { return Previous ( false ); }
	
		/// <summary>
		/// The index of the previous vertex along the path.
		/// </summary>
		/// <param name="wrap">Whether or not to consider points beyond the start of the path.</param>
		/// <returns>Index.</returns>
		public int Previous ( bool wrap )
		{
			if ( HasPrevious( wrap ) ) return index == 0 ?  shortCount - 1 : index - 1;
		
			return index;
		}

		/// <summary>
		/// The are of this path.
		/// </summary>
		public float Area
		{
			get
			{
				return area;
			}
		}
	}
}
