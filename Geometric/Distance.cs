﻿using UnityEngine;

namespace ToBoldlyPlay.Geometric
{
	public static partial class Geometry
    {
        public enum IntersectionOutcome { DISJOINT = 0, POINT = 1, LINEAR = 2, POLYGONAL = 3 };

        /// <summary>
        /// Distance between two line segments.
        /// </summary>
        /// <param name="segment1">First line segment.</param>
        /// <param name="segment2">Second line segment.</param>
        /// <param name="pointAlong1">Point on the first line segment closest to the second.</param>
        /// <param name="pointAlong2">Point on the second line segment closest to the first.</param>
        /// <returns>Distance.</returns>
        public static float Distance( Segment segment1, Segment segment2, out Vector3 pointAlong1, out Vector3 pointAlong2 )
		{
			Vector3   u = segment1.p2 - segment1.p1;
			Vector3   v = segment2.p2 - segment2.p1;
			Vector3   w = segment1.p1 - segment2.p1;

			float    a = Vector3.Dot(u,u);			// always >= 0
			float    b = Vector3.Dot(u,v);
			float    c = Vector3.Dot(v,v);			// always >= 0
			float    d = Vector3.Dot(u,w);
			float    e = Vector3.Dot(v,w);
			float    D = a*c - b*b;					// always >= 0
			float    sc, tc;
		
			// compute the line parameters of the two closest points

			if ( D < ERROR ) {				// the lines are almost parallel
				sc = 0.0f;
				tc = (b>c ? d/b : e/c);		// use the largest denominator
			}
			else {
				sc = (b*e - c*d) / D;
				tc = (a*e - b*d) / D;
			}
		
			sc = Mathf.Clamp01( sc );
			tc = Mathf.Clamp01( tc );
		
			pointAlong1 = segment1.p1 + u * sc;
			pointAlong2 = segment2.p1 + v * tc;
		
			// get the difference of the two closest points
			Vector3   dP = w + (sc * u) - (tc * v);
		
			return dP.magnitude;   // return the closest distance
		}
		
		/// <summary>
		/// Distance from a point to a line segment.
		/// </summary>
		/// <param name="segment">Line segment.</param>
		/// <param name="point">Point.</param>
		/// <returns>Distance.</returns>
		public static float Distance( Segment segment, Vector3 point )
		{
			Vector3 p;
			return Distance ( segment, point, out p );
		}
		
		/// <summary>
		/// Distance from a point to a line segment.
		/// </summary>
		/// <param name="segment">Line segment.</param>
		/// <param name="point">Point.</param>
		/// <param name="pointAlongSegment">Closest point along the line segment to the point.</param>
		/// <returns>Distance.</returns>
		public static float Distance( Segment segment, Vector3 point, out Vector3 pointAlongSegment )
		{
			Vector3 delta = segment.p2 - segment.p1;
		
			Vector3 forward = delta.normalized;
			Vector3 up = Vector3.Cross ( forward, ( point - segment.p1 ).normalized ).normalized;
			Vector3 right = Vector3.Cross ( up, forward ).normalized;
		
			pointAlongSegment = Projection( segment.p1, right, point, right );

			if ( new Plane( - forward, segment.p1 ).GetSide( pointAlongSegment ) )		pointAlongSegment = segment.p1;
			else if ( new Plane( forward, segment.p2 ).GetSide( pointAlongSegment ) )		pointAlongSegment = segment.p2;
				
			return Vector3.Distance( pointAlongSegment, point );
		}
	
		/// <summary>
		/// Distance from a path to a point.
		/// </summary>
		/// <param name="path">Path.</param>
		/// <param name="x">Point.</param>
		/// <returns>Distance.</returns>
		public static float Distance( Vector3[] path, Vector3 x )
		{
			float t;
			Vector3 p;
			return Distance ( path, x, out p, out t );
		}
		
		/// <summary>
		/// Distance from a path to a point.
		/// </summary>
		/// <param name="path">Path.</param>
		/// <param name="point">Point.</param>
		/// <param name="pointAlongPath">Closest point along path to the point.</param>
		/// <param name="displacementAlongPath">Displacement along path of closest point to point.</param>
		/// <returns>Distance.</returns>
		public static float Distance( Vector3[] path, Vector3 point, out Vector3 pointAlongPath, out float displacementAlongPath )
		{
			var distance = float.PositiveInfinity;
		
			pointAlongPath = point;
		
			displacementAlongPath = 0f;
		
			var dispalcement = 0f;
		
			for( int i = 1; i < path.Length; i++ )
			{
				Vector3 p;
			
				var d = Distance ( new Segment( path[i-1], path[i] ), point, out p );
			
				if ( d < distance )
				{
					distance = d;
					pointAlongPath = p; 
					displacementAlongPath = dispalcement + Vector3.Distance ( path[i-1], p );
				}
			
				dispalcement += Vector3.Distance ( path[i-1], path[i] );;
			}
		
			return distance;
		}
		
		/// <summary>
		/// Distance from a polygon to a point.
		/// </summary>
		/// <param name="polygon">Polygon.</param>
		/// <param name="point">Point.</param>
		/// <param name="pointInPolygon">Closest point in polygon to the point.</param>
		/// <param name="solid">Whether or not to consider the path as a solid polygon.</param>
		/// <param name="triangulation">The triangulaton function to use. Defaults to naive.</param>
		/// <returns>Distance.</returns>
		public static float Distance( Vector3[] polygon, Vector3 point, out Vector3 pointInPolygon, Triangulation.Function triangulation )
		{
			if ( triangulation == null )
			{
				triangulation = Triangulation.Naive;
			}

			Vector3 normal = polygon.Normal();

			Vector3 projection = Projection( polygon[0], normal, point, normal );

			bool contains = false;

			Triangulation.SideEffect sideEffect = ( i, j, k ) =>
			{
				if ( Contains( polygon[i], polygon[j], polygon[k], projection ) )
				{
					return contains = true;
				}

				return false;
			};

			triangulation( polygon, Functional.Series.Indices( polygon.Length ), sideEffect );

			if ( contains )
			{
				pointInPolygon = projection;

				return Vector3.Distance( point, pointInPolygon );
			}

			float d;

			return Distance( polygon, point, out pointInPolygon, out d );
		}
		
		/// <summary>
		/// Distance from a path to a line segment.
		/// </summary>
		/// <param name="path">Path.</param>
		/// <param name="segment">Line segment.</param>
		/// <returns>Distance.</returns>
		public static float Distance( Vector3[] path, Segment segment )
		{
			float t;
			Vector3 p1, p2;
			return Distance ( path, segment, out p1, out p2, out t );
		}
	
		/// <summary>
		/// Distance from a path to a line segment.
		/// </summary>
		/// <param name="path">Path.</param>
		/// <param name="segment">Line segment.</param>
		/// <param name="pointAlongPath">Closest point along the path to the segment.</param>
		/// <param name="pointAlongSegment">Closest point along the segment to the path.</param>
		/// <param name="dispalcementAlongPath">Displacement along path of closest point to segment.</param>
		/// <returns>Distance.</returns>
		public static float Distance( Vector3[] path, Segment segment, out Vector3 pointAlongPath, out Vector3 pointAlongSegment, out float dispalcementAlongPath )
		{
			var distance = float.PositiveInfinity;
		
			pointAlongPath = Vector3.zero;
		
			pointAlongSegment = Vector3.zero;
		
			dispalcementAlongPath = 0f;
		
			var coarseDisplacement = 0f;
		
			for( int i = 1; i < path.Length; i++ )
			{
				Vector3 pointAlongPathSegment, pointAlongSegmentSegment;
			
				var d = Distance ( new Segment( path[i-1], path[i] ), segment, out pointAlongPathSegment, out pointAlongSegmentSegment );
						
				if ( d < distance )
				{
					distance = d;
					pointAlongPath = pointAlongPathSegment;
					pointAlongSegment = pointAlongSegmentSegment;
					dispalcementAlongPath = coarseDisplacement + Vector3.Distance ( path[i-1], pointAlongPathSegment );
				}
			
				coarseDisplacement += Vector3.Distance ( path[i-1], path[i] );
			}
		
			return distance;
		}
	}
}
