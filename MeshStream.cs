﻿using UnityEngine;

namespace ToBoldlyPlay.Procedural
{
	public enum VertexDataFlags : byte
	{
		NONE	= 0,
		UV		= 1,
		NORMAL	= 2,
		TANGENT	= 4,
		COLOR	= 8
	};

	public class MeshStream
	{
		public void CopyTo ( Mesh mesh, bool calculateNormals = true, bool calculateTangents = true, bool calculateBounds = true, bool clear = true )
		{
			if ( clear )
			{
				mesh.Clear();
			}

			PerformCopyTo( mesh );

			if ( calculateBounds )
			{
				mesh.RecalculateBounds();
			}

			if ( calculateNormals )
			{
				mesh.RecalculateNormals();
			}

			if ( calculateTangents )
			{
				mesh.RecalculateTangents();
			}
		}

		protected abstract void PerformCopyTo ( Mesh mesh );
	}
}
